import numpy
import os

from stackexchangedata.db import DatabaseConnection
from utils.generic import list_or_single2list

from sklearn.cross_validation import train_test_split


class FeatureSource(DatabaseConnection):
    def __init__(self, db_path, selected_questions=None):
        super(FeatureSource, self).__init__(db_path)
        self.selected_questions = selected_questions
        self.init_train_test_split()

    def get_X(self):
        query = "SELECT Features FROM features"
        features = list(self.execute(query))
        features = [eval(e[0]) for e in features]
        return numpy.array(features)

    def get_y(self):
        query = "SELECT Truth FROM features"
        truths = list(self.execute(query))
        truths = [e[0] for e in truths]
        return numpy.array(truths).reshape(-1, 1)

    def get_y_score(self):
        query = "SELECT Score FROM features"
        truths = list(self.execute(query))
        truths = [e[0] for e in truths]
        return numpy.array(truths)

    def init_train_test_split(self, test_size=0.5, random_state=None):
        self.train_questions, self.test_questions = train_test_split(self.get_question_ids(), test_size=test_size, random_state=random_state)
        self.X_train = self.get_features_by_parent_id(self.train_questions)
        self.X_test = self.get_features_by_parent_id(self.test_questions)
        self.y_train = self.get_truths_by_parent_id(self.train_questions)
        self.y_test = self.get_truths_by_parent_id(self.test_questions)
        self.y_train_score = self.get_score_by_parent_id(self.train_questions)
        self.y_test_score = self.get_score_by_parent_id(self.test_questions)

    @property
    def X_train_selected(self):
        return self.get_features_by_parent_id(self.selected_questions)

    @property
    def y_train_selected(self):
        return self.get_truths_by_parent_id(self.selected_questions)

    def get_answer_ids(self):
        query = "SELECT AnswerId FROM features"
        answer_ids = list(self.execute((query)))
        answer_ids = [e[0] for e in answer_ids]
        return answer_ids

    def get_question_ids(self):
        query = "SELECT DISTINCT ParentId FROM features"
        return [p[0] for p in self.execute(query).fetchall()]

    def get_question_ids_with_min_answers(self, min_answers):
        query = "SELECT ParentId FROM features GROUP BY ParentId HAVING count(*) >= %s" % min_answers
        return [r["ParentId"] for r in self.execute(query).fetchall()]

    def get_reporting_question_ids(self, min_answers):
        questions_with_min_answers = self.get_question_ids_with_min_answers(min_answers)
        report_train_questions = list(set(questions_with_min_answers).intersection(set(self.train_questions)))
        report_test_questions = list(set(questions_with_min_answers).intersection(set(self.test_questions)))
        return report_train_questions, report_test_questions

    def get_features(self, answer_id):
        query = "SELECT Features FROM features WHERE AnswerId = %s" % answer_id
        f = self.execute(query).fetchone()
        if f is None:
            raise LookupError("Features for AnswerId = %s not available" % answer_id)
        else:
            features = eval(f[0])
            return numpy.array(list(features)).reshape(1, -1)

    def get_ground_truth(self, answer_id):
        query = "SELECT Truth FROM features WHERE AnswerId = %s" % answer_id
        truth = list(self.execute(query))[0][0]
        return truth

    def get_multiple_features(self, answer_ids):
        query = "SELECT Features FROM features WHERE AnswerId IN (%s)" % ", ".join(map(str, answer_ids))
        features = [list(eval(r[0])) for r in list(self.execute(query))]
        return features

    def get_multiple_truths(self, answer_ids):
        query = "SELECT Truth FROM features WHERE AnswerId IN (%s)" % ", ".join(map(str, answer_ids))
        truths = list(self.execute(query))
        return map(lambda t: t[0], truths)

    def get_features_by_parent_id(self, parent_ids):
        parent_ids = list_or_single2list(parent_ids)
        query = "SELECT Features FROM features WHERE ParentId IN (%s)" % ", ".join(map(str, parent_ids))
        return numpy.array([list(eval(r[0])) for r in list(self.execute(query))])

    def get_truths_by_parent_id(self, parent_ids):
        parent_ids = list_or_single2list(parent_ids)
        query = "SELECT Truth FROM features WHERE ParentId IN (%s)" % ", ".join(map(str, parent_ids))
        truths = list(self.execute(query))
        return numpy.array(map(lambda t: t[0], truths)).reshape(-1, 1)

    def get_score_by_parent_id(self, parent_ids):
        parent_ids = list_or_single2list(parent_ids)
        query = "SELECT Score FROM features WHERE ParentId IN (%s)" % ", ".join(map(str, parent_ids))
        scores = list(self.execute(query))
        return numpy.array(map(lambda t: t[0], scores)).reshape(-1, 1)

    @property
    def type(self):
        return os.path.basename(self.db_path).replace("-features.db", "")
