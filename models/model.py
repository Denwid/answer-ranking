import pickle

from sklearn import linear_model
from sklearn import tree
from sklearn import ensemble
from sklearn import svm
from sklearn import pipeline
from sklearn import preprocessing
from sklearn import grid_search


models = {
    "linear_regression": linear_model.LinearRegression(normalize=True),
    "ridge_regression": linear_model.RidgeCV(alphas=[10**i for i in range(-10, 10)], fit_intercept=False),
    "ridge_regressionCV": pipeline.Pipeline([
        ('standardization', preprocessing.StandardScaler()),
        ('estimator', grid_search.GridSearchCV(
            estimator=linear_model.Ridge(),
            param_grid=dict(alpha=[0.1, 1]),
            verbose=1
            )
        )
    ]),
    "decision_tree2": tree.DecisionTreeRegressor(max_depth=2),
    "random_forest": ensemble.RandomForestRegressor(n_estimators=1000, n_jobs=-1),
    "random_forestCV": grid_search.GridSearchCV(
        estimator=ensemble.RandomForestRegressor(),
        param_grid=dict(n_estimators=[10, 20]),
        n_jobs=-1,
        verbose=1
    ),
    "svmCV": pipeline.Pipeline([
        ('standardization', preprocessing.StandardScaler()),
        ('estimator', grid_search.GridSearchCV(
            estimator=svm.SVR(),
            param_grid=dict(C=[0.1, 1], kernel=['rbf']),
            n_jobs=-1,
            verbose=1
            )
        )
    ]),
}


fancy_cv_values = {
            "random_forestCV": dict(
                n_estimators=[10, 100, 1000],
                max_features=['auto', 'sqrt', 'log2'],
                min_samples_split=[1, 2, 10, 100, 1000]
            ),
            "svmCV": dict(
                kernel=['linear', 'rbf'],
                C=[10**i for i in range(-2, 3)],
                epsilon=[10**i for i in range(-1, 2)]
            ),
            "ridge_regressionCV": dict(
                alpha=[10**i for i in range(-10, 10)],
                fit_intercept=[True, False]
            )
}


cv_models = ["svmCV", "ridge_regression", "random_forestCV", "ridge_regressionCV"]


class Model():
    def __init__(self, model_name, train_featuresource=None):
        self.model = models[model_name]
        self.name = model_name
        if train_featuresource:
            self.train(train_featuresource)

    def __getattr__(self, item):
        return self.model.__getattribute__(item)

    def set_cv_param_grid(self, param_grid):
        if isinstance(self.model, linear_model.RidgeCV):
            self.model.alphas=param_grid
        elif isinstance(self.model, pipeline.Pipeline):
            self.model.set_params(estimator__param_grid=param_grid)
        elif isinstance(self.model, grid_search.GridSearchCV):
            self.model.param_grid=param_grid
        else:
            raise AttributeError("%s model doesn't allow to set cross validation parameter grid" % self.name)

    def make_fancy(self):
        if self.name in fancy_cv_values.keys():
            self.set_cv_param_grid(fancy_cv_values[self.name])

    def feature_importances(self):
        importances = None
        # Normal models
        try:
            importances = self.model.coef_
        except AttributeError:
            pass
        try:
            importances = self.model.feature_importances_
        except AttributeError:
            pass
        # Pipelines
        try:
            importances = self.model.named_steps['estimator'].coef_
        except AttributeError or ValueError:
            pass
        try:
            importances = self.model.named_steps['estimator'].importances_
        except AttributeError:
            pass
        # Cross-validated models
        try:
            importances = self.model.best_estimator_.feature_importances_
        except AttributeError:
            pass
        # Cross-Validated pipelines
        try:
            importances = self.model.named_steps['estimator'].best_estimator_.coef_
        except AttributeError:
            pass
        except ValueError:
            pass

        if importances is None:
            return None
        else:
            return importances.reshape(-1).tolist()

    def params(self):
        if isinstance(self.model, pipeline.Pipeline):
            return None
        else:
            try:
                return self.model.params_
            except AttributeError:
                return None

    def best_params(self):
        best_params = None
        if isinstance(self.model, linear_model.RidgeCV):
            best_params = {'alpha': self.model.alpha_}
        try:
            best_params = self.model.named_steps['estimator'].best_params_
        except AttributeError:
            pass
        try:
            best_params = self.model.best_params_
        except AttributeError:
            pass
        return best_params

    def intercept(self):
        try:
            return self.model.intercept_.tolist()
        except AttributeError:
            return None

    def train(self, feature_source, train_by_score=False, train_on_selected=False):
        if not train_on_selected:
            X = feature_source.X_train
            if train_by_score:
                y = feature_source.y_train_score.reshape(-1)
            else:
                y = feature_source.y_train.reshape(-1)
        else:
            X = feature_source.X_train_selected
            y = feature_source.y_train_selected.reshape(-1)
        self.model.fit(X, y)

