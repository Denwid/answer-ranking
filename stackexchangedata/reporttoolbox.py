import numpy as np

from utils.reporting import *


class ReportToolbox():
    def __init__(self, info_source):
        self.source = info_source

    def print_summary(self):
        print "Total number of questions: %s" % self.source.nb_questions()
        print "Total number of answers: %s" % self.source.nb_answers()

    def plot_answer_lengths(self):
        answer_lengths = np.array(self.source.get_answer_lengths())
        plot_log_histogram("Answer lengths", answer_lengths)

    def plot_answers_per_user(self, **kwargs):
        questions_per_user = np.array(self.source.get_posts_per_user(1))
        answers_per_user = np.array(self.source.get_posts_per_user(2))
        plot_log_histogram("Posts per user", [questions_per_user, answers_per_user], histtype='bar', log=True, label=['Questions', 'Answers'], **kwargs)

    def plot_answers_per_question(self, **kwargs):
        answers_per_question = np.array(self.source.get_answers_per_question())
        plot_log_histogram("Empirical Distribution  of Answers per Question", answers_per_question, histtype='bar', **kwargs)

    def plot_answer_timings(self, answer_number):
        answer_timings = np.array(self.source.get_answer_timings(answer_number))
        plot_log_histogram("Time until answer number %s (Minutes)" % answer_number, answer_timings)

    def plot_answer_score(self):
        answer_scores = np.array(self.source.get_answer_scores())
        plot_semilogy_histogram("Answer scores", answer_scores)

    def plot_comments_per_answer(self):
        comment_counts = np.array(self.source.get_comments_per_answer())
        m = np.mean(comment_counts)
        std = np.std(comment_counts)
        plot_histogram("Comments per answer (mean = %1.2f, std = %1.2f)" % (m, std), comment_counts)

    def plot_viewcount_vs_total_votes(self, **kwargs):
        vc, tv = self.source.get_viewcount_vs_total_votes()
        plot_binned_statistic("View count of question vs.  Number of votes per answer", vc, tv, **kwargs)

    def plot_age_vs_total_votes(self, **kwargs):
        age, tv = self.source.get_age_vs_totalvotes()
        plot_binned_statistic("Age of answer [days] vs. Average number of votes per answer", age, tv, **kwargs)

    def plot_score_vs_time(self, xmin=0, xmax=1, logx=False, bins=100, ylim=[0, 5], statistic='mean'):
        # range be like  [[xmin, xmax], [ymin, ymax]]
        scores, times = self.source.get_score_vs_time()
        plot_binned_statistic(ur"Creation Date after Question [days] vs. Scores of Answers", times, scores, bins=bins, xmin=xmin, xmax=xmax, logx=logx, ylim=ylim, statistic=statistic)
