

class AnswererFeaturesProvider(object):
    def __init__(self, answer):
        self.answer = answer
        self.answerer = answer.get_corresponding_answerer()

    def get_relative_score(self):
            return float(self.answerer.score_received)/(self.answerer.votes_received+1)

    def get_answers_written(self):
        return self.answerer.answers_written

    def get_score(self):
        return self.answerer.score_received

    def get_votes(self):
        return self.answerer.votes_received

    def get_reports_received(self):
        if self.answerer.reports_received == None:
            return 0
        else:
            return self.answerer.reports_received

    def get_reports_received_per_answer(self):
        if self.answerer.reports_received is None or self.answerer.answers_written == 0:
            return 0
        else:
            return self.answerer.reports_received/self.answerer.answers_written

    def get_fraction_of_accepted_answers(self):
        if self.answerer.answers_written == 0:
            return 0
        else:
            return float(self.answerer.answers_accepted)/self.answerer.answers_written


class AskerFeaturesProvider(object):
    def __init__(self, question):
        self.user = question.get_corresponding_asker()
        self.answers_received = self.user.get_corresponding_answers_received(question_selector="knownquestions")
        self.questions_asked = self.user.get_corresponding_questions_asked(question_selector="knownquestions")

    def get_total_answers_received(self):
        return len(self.answers_received)

    def get_answers_received_per_question(self):
        if len(self.questions_asked) == 0:
            return 0
        else:
            return self.get_total_answers_received()/len(self.questions_asked)

    def get_received_answers_average_score(self):
        if len(self.answers_received) == 0:
            return 0
        else:
            return float(sum([a.score for a in self.answers_received]))/len(self.answers_received)

    def get_reports_received(self):
        return self.user.reports_received

    def get_received_answers_total_length(self):
        return sum([len(a.body) for a in self.answers_received])

    def get_average_answers_received_with_votes(self):
        if len(self.answers_received) == 0:
            return 0
        nb_questions_asked = len(self.questions_asked)
        nb_answers_with_votes = sum([1 for a in self.answers_received if a.total_votes > 0])
        return nb_answers_with_votes/nb_questions_asked
