from numpy import dot

from utils.languageprocessing import *
from utils.languageprocessing import wordset


class TextFeaturesProvider(object):
    def __init__(self, s):
        self.s = s # the plain string
        self.sf = filter_out_rich_content(s)

    def length(self):
        return len(self.s)

    def nb_code_blocks(self, relative=False):
        nb_code_blocks = self.s.count('<code>')
        if relative:
            return float(nb_code_blocks)/self.length()
        else:
            return nb_code_blocks

    def nb_images(self, relative=False):
        nb_images = self.s.count('<img')
        if relative:
            return float(nb_images)/self.length()
        else:
            return nb_images

    def nb_latex_blocks(self, relative=False):
        double_dollar_blocks = self.s.count('$$')/2
        dollar_blocks = self.s.count('$')/2 - 2*double_dollar_blocks
        bracket_blocks = self.s.count('\[')
        parentheses_blocks = self.s.count('\(')
        nb_latex_blocks = double_dollar_blocks + dollar_blocks + bracket_blocks + parentheses_blocks
        if relative:
            return float(nb_latex_blocks)/self.length()
        else:
            return nb_latex_blocks

    def kincaid(self):
        """ Kincaid (readability) score of the given text

        Formula from https://en.wikipedia.org/w/index.php?title=Flesch%E2%80%93Kincaid_readability_tests&oldid=705963940
        """
        nb_sentences = len(nltk.sent_tokenize(self.sf))
        words = wordlist(self.sf, filter_punctuation=True)
        nb_words = len(words)
        nb_syllables = sum([count_syllables(w) for w in words])
        if nb_words == 0:
            return 206.835
        else:
            return 206.835 - 1.015*(float(nb_words)/nb_sentences) - 84.6*(float(nb_syllables)/nb_words)

    def nb_long_words(self, threshold=6, relative=False):
        words = wordlist(self.sf, filter_punctuation=True)
        nb_words = len(words)
        nb_long_words = len([w for w in words if len(w) > threshold])
        if relative:
            relative_nb_long_words = 0 if nb_words == 0 else float(nb_long_words)/nb_words
            return relative_nb_long_words
        else:
            return nb_long_words

    def nb_unique_words(self, relative=False):
        nb_unique_words = len(wordset(self.sf, filter_punctuation=True))
        if relative:
            nb_words = len(wordlist(self.sf, filter_punctuation=True))
            relative_nb_unique_words = 0 if nb_words == 0 else float(nb_unique_words)/nb_words
            return relative_nb_unique_words
        else:
            return nb_unique_words

    def entropy(self, languagemodel):
        return languagemodel.entropy(self.sf)

    def doc2vec_complete(self, answer_id, doc2vec_model):
        return tuple(doc2vec_model.docvecs["ANSWER_%s" % answer_id])


class TextComparisionFeaturesProvider(object):
    def __init__(self, s1="", s2=""):
        self.s1 = s1
        self.s1f = filter_out_rich_content(s1)
        self.s2 = s2
        self.s2f = filter_out_rich_content(s2)

    def word_overlap(self):
        return len(wordset(self.s1f) & wordset(self.s2f))

    def length_ratio(self):
        return float(len(self.s1))/len(self.s2)

    def similarity_to_question(self, answer_id, question_id, doc2vec_model):
        if doc2vec_model is None:
            return 0
        from gensim.models import Doc2Vec
        from gensim import matutils
        assert(isinstance(doc2vec_model, Doc2Vec))
        answer_vector = doc2vec_model.docvecs["ANSWER_%s" % answer_id]
        question_vector = doc2vec_model.docvecs["QUESTION_%s" % question_id]
        return dot(matutils.unitvec(answer_vector), matutils.unitvec(question_vector))

    def colinearity_with_general_qa_pair(self, answer_id, question_id, doc2vec_model):
        if doc2vec_model is None:
            return 0
        from gensim.models import Doc2Vec
        from gensim import matutils
        assert(isinstance(doc2vec_model, Doc2Vec))
        answer_vector = doc2vec_model.docvecs["ANSWER_%s" % answer_id]
        question_vector = doc2vec_model.docvecs["QUESTION_%s" % question_id]
        general_answer_vector = doc2vec_model.docvecs["ANSWER"]
        general_question_vector = doc2vec_model.docvecs["QUESTION"]
        qa_direction = question_vector - answer_vector
        general_qa_direction = general_question_vector - general_answer_vector
        return dot(matutils.unitvec(qa_direction), matutils.unitvec(general_qa_direction))