from textfeatures import *
from higherfeatures import *

from utils.settings import get_db

def get_feature_set(name, dataset_name=None):
    from gensim.models import Doc2Vec
    from utils.ngram import NgramModel
    try:
        doc2vec_model = Doc2Vec.load(get_db(dataset_name, "doc2vec"))
    except IOError:
        print "Warning: Doc2Vec features will be set to 0, since no doc2vec model is available"
        doc2vec_model = None
    unigram_model = NgramModel.load_from_file(get_db(dataset_name, "unigram"))
    trigram_model = NgramModel.load_from_file(get_db(dataset_name, "trigram"))
    if name is "epfl1features":
        return EPFL1Features(doc2vec_model)
    elif name is "halfofagichteinfeatures":
        return HalfOfAgichteinFeatures()
    elif name is "agichteinfeatures":
        return AgichteinFeatures(unigram_model, trigram_model)
    elif name is "epfl2features":
        return EPFL2Features(doc2vec_model)
    elif name is "epfl3features":
        return EPFL3Features(doc2vec_model)
    elif name is "epfl4features":
        return EPFL4Features(doc2vec_model, unigram_model, trigram_model)
    elif name is "epfl5features":
        return EPFL5Features(doc2vec_model, unigram_model, trigram_model)
    elif name is "epfl6features":
        return EPFL6Features(doc2vec_model, unigram_model, trigram_model)
    elif name is "epfl7features":
        return EPFL7Features(doc2vec_model, unigram_model, trigram_model)
    elif name is "doc2vecfeatures":
        return Doc2VecFeatures(doc2vec_model)
    else:
        raise ValueError("get_feature_set() does not know a featureset with name %s" % name)


def featuresource2labels(feature_source):
    t = feature_source.type
    if t == "half-agichtein":
        return HalfOfAgichteinFeatures.labels()
    elif t == "agichtein":
        return AgichteinFeatures.labels()
    elif t == "epfl1":
        return EPFL1Features.labels()
    elif t == "epfl2":
        return EPFL2Features.labels()
    elif t == "epfl3":
        return EPFL3Features.labels()
    elif t == "epfl4":
        return EPFL4Features.labels()
    elif t == "epfl5":
        return EPFL5Features.labels()
    elif t == "epfl6":
        return EPFL6Features.labels()
    elif t == "epfl7":
        return EPFL7Features.labels()
    elif t == "doc2vec":
        return Doc2VecFeatures.labels()
    else:
        raise LookupError("Don't know labels of feature_source.type %s" % t)


class Features(object):
    def from_answer(self, answer):
        raise NotImplementedError()

    def get_ground_truth(self, answer):
        return answer.get_ground_truth()

    def name(self):
        return str(type(self).__name__).lower()


class HalfOfAgichteinFeatures(Features):
    def from_answer(self, answer):
        answerer_features = AnswererFeaturesProvider(answer)
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            text_features.length(),
            text_features.nb_long_words(),
            answerer_features.get_relative_score(),
            # trigram model entropy,
            answerer_features.get_fraction_of_accepted_answers(),
            text_features.nb_unique_words(),
            answerer_features.get_reports_received(),
            comparison_features.word_overlap(),
            text_features.kincaid()
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        question_body = extended_answer["QuestionBody"]
        text_features = TextFeaturesProvider(answer_body)
        comparision_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            text_features.length(),
            text_features.nb_long_words(),
            extended_answer["AnswererRelativeScoreReceived"],
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            text_features.nb_unique_words(),
            extended_answer["AnswererReportsReceived"],
            comparision_features.word_overlap(),
            text_features.kincaid()
        )

    @staticmethod
    def labels():
        return [
            "A-len",
            "A-words",
            "A-rel.score",
            "AU-%accecpted Answers",
            "A-unique words",
            "AU-reports",
            "AvQ-word overlap",
            "A-kincaid"
        ]

#Use in report
class AgichteinFeatures(Features):
    def __init__(self, unigram_model, trigram_model):
        self.unigram_model = unigram_model
        self.trigram_model = trigram_model

    def from_answer(self, answer):
        answerer_features = AnswererFeaturesProvider(answer)
        question = answer.get_corresponding_question()
        asker_features = AskerFeaturesProvider(question)
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            text_features.length(),
            text_features.nb_long_words(),
            answerer_features.get_relative_score(),
            text_features.entropy(self.trigram_model),
            answerer_features.get_fraction_of_accepted_answers(),
            text_features.nb_unique_words(),
            answerer_features.get_reports_received_per_answer(),
            comparison_features.word_overlap(),
            text_features.kincaid(),
            asker_features.get_answers_received_per_question(),
            comparison_features.length_ratio(),
            answerer_features.get_score(),
            # AskerAverageVotesOnAnswers but ambigous description,
            text_features.entropy(self.unigram_model),
            # KL-divergence between answer and wikipedia model,
            asker_features.get_reports_received(),
            asker_features.get_received_answers_total_length(),
            # asker_received_answers_total_negative_votes,
            asker_features.get_average_answers_received_with_votes()
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            text_features.length(),
            text_features.nb_long_words(),
            extended_answer["AnswererRelativeScoreReceived"],
            text_features.entropy(self.trigram_model),
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            text_features.nb_unique_words(),
            extended_answer["AnswererReportsReceivedPerAnswer"],
            comparison_features.word_overlap(),
            text_features.kincaid(),
            extended_answer["AskerAnswersReceivedPerQuestion"],
            comparison_features.length_ratio(),
            extended_answer["AnswererScoreReceived"],
            #Something like AskerAverageVotesOnAnswers but ambigous description,
            text_features.entropy(self.unigram_model),
            #KL-divergence between answer's and a wikipedia language model,
            extended_answer["AskerReportsReceived"],
            extended_answer["AskerAnswersReceivedTotalLength"],
            #AskerOtherAnswersDownvotesReceived,
            extended_answer["AskerAverageAnswersReceivedWithVotes"],
        )

    @staticmethod
    def labels():
        return [
            "Answer - length",
            "Answer - #long words",
            "Answerer - NormalizedScoreReceived",
            "Answer - 3gram model entropy",
            "Answerer - FractionOfAcceptedAnswers",
            "Answer - #unique words",
            "Answerer - ReportsReceivedPerAnswer",
            "AvsQ - word overlap",
            "Answer - Kincaid score",
            "Asker - AnswersReceivedPerQuestion",
            "AvsQ - length ratio",
            "Answerer - ScoreReceived",
            "Answer - 1gram model entropy",
            "Asker - ReportsReceived",
            "Asker - AnswersReceivedTotalLength",
            "Asker - AverageAnswersReceivedWithVotes"
        ]


class EPFL1Features(Features):
    def __init__(self, doc2vec_model=None):
         self.doc2vec_model = doc2vec_model

    def from_answer(self, answer):
        answerer_features = AnswererFeaturesProvider(answer)
        question = answer.get_corresponding_question()
        comments = answer.get_corresponding_comments()
        asker_features = AskerFeaturesProvider(question)
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(),
            text_features.nb_unique_words(),
            text_features.kincaid(),
            text_features.nb_latex_blocks(), #ours
            text_features.nb_images(), #ours
            text_features.nb_code_blocks(), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            # ---- Answerer features ---- #
            answerer_features.get_relative_score(),
            answerer_features.get_fraction_of_accepted_answers(),
            answerer_features.get_reports_received(),
            answerer_features.get_score(),
            # ---- Asker features ---- #
            asker_features.get_total_answers_received(),
            asker_features.get_received_answers_average_score(),
            asker_features.get_reports_received(),
            asker_features.get_received_answers_total_length(),
            asker_features.get_average_answers_received_with_votes(),
            # ---- Features from comments ---- #
            comments.get_nb_comments(), # ours
            comments.get_average_score() # ours
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(),
            text_features.nb_unique_words(),
            text_features.kincaid(),
            text_features.nb_latex_blocks(), #ours
            text_features.nb_images(), #ours
            text_features.nb_code_blocks(), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            # ---- Answerer features ---- #
            extended_answer["AnswererRelativeScoreReceived"],
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            extended_answer["AnswererReportsReceived"],
            extended_answer["AnswererScoreReceived"],
            # ---- Asker features ---- #
            extended_answer["AskerAnswersReceived"],
            extended_answer["AskerAnswersReceivedAverageScore"],
            extended_answer["AskerReportsReceived"],
            extended_answer["AskerAnswersReceivedTotalLength"],
            extended_answer["AskerAverageAnswersReceivedWithVotes"],
            # ---- Features from comments ---- #
            extended_answer["Comments"],
            extended_answer["AverageCommentScore"]
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - #long words",
            "Answer - #unique words",
            "Answer - Kincaid score",
            "Answer - #latex blocks", #ours
            "Answer - #images", #ours
            "Answer - #code blocks", #ours
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio",
            # ---- Answerer features ---- #
            "Answerer - NormalizedScoreReceived",
            "Answerer - FractionOfAcceptedAnswers",
            "Answerer - ReportsReceived",
            "Answerer - ScoreReceived",
            # ---- Asker features ---- #
            "Asker - AnswersReceived",
            "Asker - AnswersReceivedAverageScore",
            "Asker - ReportsReceived",
            "Asker - AnswersReceivedTotalLength",
            "Asker - AverageAnswersReceivedWithVotes",
            # ---- Features from comments ---- #
            "Comments",
            "Comments - AverageCommentScore"
        ]


#Just text features
class EPFL2Features(Features):
    def __init__(self, doc2vec_model):
        self.doc2vec_model = doc2vec_model

    def from_answer(self, answer):
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(),
            text_features.nb_unique_words(),
            text_features.kincaid(),
            text_features.nb_latex_blocks(), #ours
            text_features.nb_images(), #ours
            text_features.nb_code_blocks(), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio()
        )


    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(),
            text_features.nb_unique_words(),
            text_features.kincaid(),
            text_features.nb_latex_blocks(), #ours
            text_features.nb_images(), #ours
            text_features.nb_code_blocks(), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio()
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - #long words",
            "Answer - #unique words",
            "Answer - Kincaid score",
            "Answer - #latex blocks", #ours
            "Answer - #images", #ours
            "Answer - #code blocks", #ours
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio"
        ]

#Just text features but normalized
class EPFL3Features(Features):
    def __init__(self, doc2vec_model):
        self.doc2vec_model = doc2vec_model

    def from_answer(self, answer):
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio()
        )


    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio()
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - rel. #long words",
            "Answer - rel. #unique words",
            "Answer - Kincaid score",
            "Answer - rel. #latex blocks", #ours
            "Answer - rel. #images", #ours
            "Answer - rel. #code blocks", #ours
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio"
        ]

# Use in report
# Just text features but normalized and with colinearity and with uni- and tri-gram models
class EPFL4Features(Features):
    def __init__(self, doc2vec_model, unigram_model, trigram_model):
        self.doc2vec_model = doc2vec_model
        self.unigram_model = unigram_model
        self.trigram_model = trigram_model

    def from_answer(self, answer):
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer.post_id, answer.parent_id, self.doc2vec_model)
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer_id, question_id, self.doc2vec_model)
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - rel. #long words",
            "Answer - rel. #unique words",
            "Answer - Kincaid score",
            "Answer - rel. #latex blocks", #ours
            "Answer - rel. #images", #ours
            "Answer - rel. #code blocks", #ours
            "Answer - 1gram model entropy",
            "Answer - 3gram model entropy",
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio",
            "AvsQ - doc2vec collinearity"
        ]

# All features but "easy" ones removed
class EPFL5Features(Features):
    def __init__(self, doc2vec_model, unigram_model, trigram_model):
        self.doc2vec_model = doc2vec_model
        self.unigram_model = unigram_model
        self.trigram_model = trigram_model

    def from_answer(self, answer):
        answerer_features = AnswererFeaturesProvider(answer)
        question = answer.get_corresponding_question()
        asker_features = AskerFeaturesProvider(question)
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, answer.get_corresponding_question().body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer.post_id, answer.parent_id, self.doc2vec_model),
            # ---- Answerer features ---- #
            answerer_features.get_fraction_of_accepted_answers(),
            answerer_features.get_reports_received(),
            # ---- Asker features ---- #
            asker_features.get_total_answers_received(),
            asker_features.get_reports_received(),
            asker_features.get_received_answers_total_length(),
            asker_features.get_average_answers_received_with_votes()
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer_id, question_id, self.doc2vec_model),
            # ---- Answerer features ---- #
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            extended_answer["AnswererReportsReceived"],
            # ---- Asker features ---- #
            extended_answer["AskerAnswersReceived"],
            extended_answer["AskerReportsReceived"],
            extended_answer["AskerAnswersReceivedTotalLength"],
            extended_answer["AskerAverageAnswersReceivedWithVotes"]
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - rel. #long words",
            "Answer - rel. #unique words",
            "Answer - Kincaid score",
            "Answer - rel. #latex blocks", #ours
            "Answer - rel. #images", #ours
            "Answer - rel. #code blocks", #ours
            "Answer - 1gram model entropy",
            "Answer - 3gram model entropy",
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio",
            "AvsQ - doc2vec collinearity",
            # ---- Answerer features ---- #
            "Answerer - FractionOfAcceptedAnswers",
            "Answerer - ReportsReceived",
            # ---- Asker features ---- #
            "Asker - AnswersReceived",
            "Asker - ReportsReceived",
            "Asker - AnswersReceivedTotalLength",
            "Asker - AverageAnswersReceivedWithVotes"
        ]


# Use in report
# All features from Agichtein+Our own features (text & social & comments, text normalized) --> Everything
#EPFL6
class EPFL6Features(Features):
    def __init__(self, doc2vec_model, unigram_model, trigram_model):
        self.doc2vec_model = doc2vec_model
        self.unigram_model = unigram_model
        self.trigram_model = trigram_model

    def from_answer(self, answer):
        answerer_features = AnswererFeaturesProvider(answer)
        question = answer.get_corresponding_question()
        comments = answer.get_corresponding_comments()
        asker_features = AskerFeaturesProvider(question)
        text_features = TextFeaturesProvider(answer.body)
        comparison_features = TextComparisionFeaturesProvider(answer.body, question.body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer.post_id, answer.parent_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer.post_id, answer.parent_id, self.doc2vec_model),
            # ---- Answerer features ---- #
            answerer_features.get_answers_written(),
            answerer_features.get_score(),
            answerer_features.get_relative_score(),
            answerer_features.get_fraction_of_accepted_answers(),
            answerer_features.get_reports_received(),
            # ---- Asker features ---- #
            asker_features.get_total_answers_received(),
            asker_features.get_received_answers_average_score(),
            asker_features.get_reports_received(),
            asker_features.get_received_answers_total_length(),
            asker_features.get_average_answers_received_with_votes(),
            # ---- Features from comments ---- #
            comments.get_nb_comments(), # ours
            comments.get_average_score() # ours
        )

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer_id, question_id, self.doc2vec_model),
            # ---- Answerer features ---- #
            extended_answer["AnswererAnswersWritten"],
            extended_answer["AnswererScoreReceived"],
            extended_answer["AnswererRelativeScoreReceived"],
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            extended_answer["AnswererReportsReceived"],
            # ---- Asker features ---- #
            extended_answer["AskerAnswersReceived"],
            extended_answer["AskerAnswersReceivedAverageScore"],
            extended_answer["AskerReportsReceived"],
            extended_answer["AskerAnswersReceivedTotalLength"],
            extended_answer["AskerAverageAnswersReceivedWithVotes"],
            # ---- Features from comments ---- #
            extended_answer["Comments"],
            extended_answer["AverageCommentScore"]
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - rel. #long words",
            "Answer - rel. #unique words",
            "Answer - Kincaid score",
            "Answer - rel. #latex blocks", #ours
            "Answer - rel. #images", #ours
            "Answer - rel. #code blocks", #ours
            "Answer - 1gram model entropy",
            "Answer - 3gram model entropy",
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio",
            "AvsQ - doc2vec collinearity",
            # ---- Answerer features ---- #
            "Answerer - AnswersWritten",
            "Answerer - ScoreReceived",
            "Answerer - NormalizedScoreReceived",
            "Answerer - FractionOfAcceptedAnswers",
            "Answerer - ReportsReceived",
            # ---- Asker features ---- #
            "Asker - AnswersReceived",
            "Asker - AnswersReceivedAverageScore",
            "Asker - ReportsReceived",
            "Asker - AnswersReceivedTotalLength",
            "Asker - AverageAnswersReceivedWithVotes",
            # ---- Features from comments ---- #
            "Comments", #ours
            "Comments - AverageCommentScore" #ours
        ]


class EPFL7Features(Features):
    def __init__(self, doc2vec_model, unigram_model, trigram_model):
        self.doc2vec_model = doc2vec_model
        self.unigram_model = unigram_model
        self.trigram_model = trigram_model

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        question_body = extended_answer["QuestionBody"]
        question_id = extended_answer["ParentID"]
        text_features = TextFeaturesProvider(answer_body)
        comparison_features = TextComparisionFeaturesProvider(answer_body, question_body)
        return (
            # ---- Features directly from body of answer ---- #
            text_features.length(),
            text_features.nb_long_words(relative=True),
            text_features.nb_unique_words(relative=True),
            text_features.kincaid(),
            text_features.nb_latex_blocks(relative=True), #ours
            text_features.nb_images(relative=True), #ours
            text_features.nb_code_blocks(relative=True), #ours
            text_features.entropy(self.unigram_model),
            text_features.entropy(self.trigram_model),
            # ---- Features form answer vs. question comparision ---- #
            comparison_features.word_overlap(),
            comparison_features.similarity_to_question(answer_id, question_id, self.doc2vec_model), #ours
            comparison_features.length_ratio(),
            comparison_features.colinearity_with_general_qa_pair(answer_id, question_id, self.doc2vec_model),
            # ---- Features from timing ---- #
            extended_answer["TimeAfterQuestion"],
            # ---- Answerer features ---- #
            extended_answer["AnswererAnswersWritten"],
            extended_answer["AnswererScoreReceived"],
            extended_answer["AnswererRelativeScoreReceived"],
            extended_answer["AnswererFractionOfAcceptedAnswers"],
            extended_answer["AnswererReportsReceived"],
            # ---- Asker features ---- #
            extended_answer["AskerAnswersReceived"],
            extended_answer["AskerAnswersReceivedAverageScore"],
            extended_answer["AskerReportsReceived"],
            extended_answer["AskerAnswersReceivedTotalLength"],
            extended_answer["AskerAverageAnswersReceivedWithVotes"],
            # ---- Features from comments ---- #
            extended_answer["Comments"],
            extended_answer["AverageCommentScore"]
        )

    @staticmethod
    def labels():
        return [
            # ---- Features directly from body of answer ---- #
            "Answer - length",
            "Answer - rel. #long words",
            "Answer - rel. #unique words",
            "Answer - Kincaid score",
            "Answer - rel. #latex blocks", #ours
            "Answer - rel. #images", #ours
            "Answer - rel. #code blocks", #ours
            "Answer - 1gram model entropy",
            "Answer - 3gram model entropy",
            # ---- Features form answer vs. question comparision ---- #
            "AvsQ - word overlap",
            "AvsQ - doc2vec similatiry", #ours
            "AvsQ - length ratio",
            "AvsQ - doc2vec collinearity",
            # ---- Features from timing ---- #
            "AvsQ - time difference",
            # ---- Answerer features ---- #
            "Answerer - AnswersWritten",
            "Answerer - ScoreReceived",
            "Answerer - NormalizedScoreReceived",
            "Answerer - FractionOfAcceptedAnswers",
            "Answerer - ReportsReceived",
            # ---- Asker features ---- #
            "Asker - AnswersReceived",
            "Asker - AnswersReceivedAverageScore",
            "Asker - ReportsReceived",
            "Asker - AnswersReceivedTotalLength",
            "Asker - AverageAnswersReceivedWithVotes",
            # ---- Features from comments ---- #
            "Comments", #ours
            "Comments - AverageCommentScore" #ours
        ]
# Small feature set with (2 most important text features) + (2 most important social features) + (colinearity)
#TODO

class Doc2VecFeatures(Features):
    def __init__(self, doc2vec_model):
        self.doc2vec_model = doc2vec_model

    def from_answer(self, answer):
        answer_body = answer.body
        answer_id = answer.id
        text_features = TextFeaturesProvider(answer_body)
        return text_features.doc2vec_complete(answer_id, self.doc2vec_model)

    def from_extended_answer(self, extended_answer):
        answer_body = extended_answer["Body"]
        answer_id = extended_answer["PostId"]
        text_features = TextFeaturesProvider(answer_body)
        return text_features.doc2vec_complete(answer_id, self.doc2vec_model)

    @staticmethod
    def labels():
        n_features = 100
        return [str(i) for i in range(n_features)]