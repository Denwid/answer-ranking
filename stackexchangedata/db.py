from utils.datetimeprocessing import timestamps2timedeltas
from utils.sqlite import DatabaseConnection
from utils.languageprocessing import wordlist, tags2list, filter_out_rich_content
from stackexchangedata.objects import *


class DataObjectSource(DatabaseConnection):
    def load(self, query, allow_empty=False):
        rows = self.execute(query).fetchall()
        if len(rows) == 0 and not allow_empty:
            raise LookupError("Query returned empty result: %s" % query)
        return rows

    def get_answer(self, answer_id):
        query = "SELECT * FROM extendedposts WHERE PostId == %s AND PostTypeId == 2" % answer_id
        return Answer(self.load(query)[0], self)

    def get_answer_set(self, question_id):
        query = "SELECT * FROM extendedposts WHERE ParentID == %s AND PostTypeId == 2" % question_id
        return AnswerSet(self.load(query, allow_empty=True), self)

    def get_answer_sets(self, question_ids):
        query = "SELECT * FROM extendedposts WHERE ParentID IN (%s)" % ", ".join(map(str, question_ids))
        r = {}
        for answerrow in self.c.execute(query).fetchall():
            question_id = answerrow["ParentId"]
            entry = r.get(question_id, [])
            entry.append(answerrow)
            r[question_id] = entry
        answer_sets = []
        for question_id, answerrows in r.items():
            answer_sets.append(AnswerSet(answerrows, self))
        return answer_sets

    def get_answers_given_to_user(self, user_id, question_selector=None):
        questions_of_this_user = "SELECT id FROM posts WHERE OwnerUserId == %s AND PostTypeId == 1" % user_id
        query = "SELECT * FROM extendedposts WHERE ParentId IN (%s)" % questions_of_this_user
        if question_selector is not None:
            query = "%s AND ParentId IN (SELECT Id FROM %s)" % (query, question_selector)
        return AnswerSet(self.load(query, allow_empty=True), self)

    def get_question(self, question_id):
        query = "SELECT * FROM posts WHERE Id == %s AND PostTypeId == 1" % question_id
        return Question(self.load(query)[0], self)

    def get_questions_asked_by_user(self, user_id, question_selector=None):
        query = "SELECT * FROM extendedposts WHERE OwnerUserId = %s AND PostTypeId = 1" % user_id
        if question_selector is not None:
            query = "%s AND PostId IN (SELECT Id FROM %s)" % (query, question_selector)
        return QuestionSet(self.load(query, allow_empty=True), self)

    def get_extended_answer(self, answer_id):
        query = "SELECT * FROM extendedanswers WHERE PostId == %s" % answer_id
        return ExtendedAnswer(self.load(query)[0], self)

    def get_user(self, user_id):
        if user_id is None or user_id == 0:
            query = """
                    SELECT
                        0 AS Id,
                        1 AS Reputation,
                        0 AS Age,
                        0 AS UpVotes,
                        0 AS DownVotes,
                        0 AS VotesReceived,
                        0 AS ScoreReceived,
                        0 AS ReportsReceived,
                        0 AS AnswersWritten,
                        0 AS AnswersAccepted
                    """
            return User(self.load(query)[0], self)
        else:
            query =  """
                    SELECT
                        users.Id,
                        users.Reputation,
                        users.Age,
                        users.UpVotes,
                        users.DownVotes,
                        answerers.VotesReceived,
                        answerers.ScoreReceived,
                        answerers.ReportsReceived,
                        answerers.AnswersWritten,
                        answerers.AnswersAccepted
                    FROM users
                        LEFT JOIN answerers ON users.Id == answerers.UserId
                    WHERE Id == %s
                    """ % user_id
            return User(self.load(query)[0], self)

    def get_comment(self, comment_id):
        query = "SELECT * FROM comments WHERE Id = %s" % comment_id
        return Comment(self.load(query)[0], self)

    def get_comment_set(self, post_id):
        query = "SELECT * FROM comments WHERE PostId = %s" % post_id
        return CommentSet(self.load(query, allow_empty=True), self)

    def get_corresponding_info_source(self):
        return InfoSource(db_path=self.db_path)


class InfoSource(DatabaseConnection):
    """Overview information on the data source"""

    def nb_posts(self, post_type_id):
        count_query = "SELECT count(*) FROM posts WHERE PostTypeId == %s" % post_type_id
        count = [c for c in self.c.execute(count_query)]
        return count[0][0]

    def nb_questions(self):
        return self.nb_posts(post_type_id=1)

    def nb_answers(self):
        return self.nb_posts(post_type_id=2)

    def get_answer_lengths(self):
        query = "SELECT length(Body) FROM posts WHERE PostTypeId == 2"
        return list(self.c.execute(query))

    def get_posts_per_user(self, post_type_id=None):
        if post_type_id is None:
            selector = ""
        else:
            selector = "WHERE PostTypeId == %s" % post_type_id
        query = "SELECT count(*) FROM posts %s GROUP BY OwnerUserId" % selector
        return map(tuple2single, list(self.c.execute(query)))

    def get_answers_per_question(self):
        query = "SELECT count(*) FROM posts WHERE PostTypeId == 2 GROUP BY ParentID"
        return list(self.c.execute(query))

    def get_answer_timings(self, answer_number=1):
        timings = []
        for timedeltas in self.get_question_answer_timedeltas().itervalues():
            if (len(timedeltas) > answer_number and timedeltas[answer_number] > 0):
                timings.append(timedeltas[answer_number])
        return timings

    def get_question_answer_timedeltas(self):
        return {k: timestamps2timedeltas(v) for k, v in self.get_question_answer_timings().iteritems()}

    def get_question_answer_timings(self):
        query = "SELECT ParentId, CreationDate FROM posts WHERE PostTypeId == 2"
        question_answer_timing_pairs = self.c.execute(query)
        question_answer_timing_pairs = list(question_answer_timing_pairs)

        question_answer_timings = dict(self.get_question_creation_date_tuples())
        question_answer_timings = {k: [v] for k, v in question_answer_timings.iteritems()}

        for (parent_id, answer_time) in question_answer_timing_pairs:
            question_answer_timings[parent_id].append(answer_time)

        return question_answer_timings

    def get_question_creation_date_tuples(self):
        query = "SELECT Id, CreationDate FROM posts WHERE PostTypeId == 1"
        dates = self.c.execute(query)
        return list(dates)

    def get_question_ids(self):
        result = list(self.execute("SELECT Id FROM posts WHERE PostTypeId == 1"))
        return map(tuple2single, result)

    def get_question_ids_of_questions_with_answers(self):
        result = list(self.execute("SELECT ParentId FROM posts WHERE PostTypeId = 2 GROUP BY ParentId"))
        return map(tuple2single, result)

    def get_ids_of_questions_with_at_least_n_answers(self, n):
        result = list(self.execute("SELECT count(Id), ParentId FROM posts WHERE PostTypeId = 2 GROUP BY ParentId;"))
        question_ids = []
        for nb_answers, question_id in result:
            if nb_answers >= n:
                question_ids.append(question_id)
        return question_ids

    def get_ids_of_questions_with_one_voted_answer(self, min_votes):
        sql = """
        SELECT
            QuestionId
        FROM
            questions q
        INNER JOIN
            extendedposts a ON q.QuestionId == a.PostId
        WHERE
            q.Answers = 1 AND
            a.TotalVotes >= %s
        """ % min_votes
        result = list(self.execute(sql))
        return map(tuple2single, result)

    def get_answer_ids(self):
        result = list(self.execute("SELECT Id FROM posts WHERE PostTypeId == 2"))
        return map(tuple2single, result)

    def get_answer_scores(self):
        query = "SELECT score FROM posts WHERE PostTypeId == 2"
        return list(self.execute(query))

    def get_comments_per_answer(self):
        nb_comments_query = "SELECT PostId, count(*) AS nbComments FROM comments GROUP BY PostId"
        query = "SELECT nbComments FROM posts LEFT JOIN (%s) ON posts.Id = PostId WHERE PostTypeId = 2;" % nb_comments_query
        return map(none2zero, map(tuple2single, list(self.execute(query))))

    def get_corresponding_data_object_source(self):
        return DataObjectSource(self.db_path)

    def get_viewcount_vs_total_votes(self):
        query = """
        SELECT
            ViewCount,
            avg(AvgTotalVotes)
        FROM (
            SELECT
                q.ViewCount AS ViewCount,
                avg(TotalVotes) AS AvgTotalVotes
            FROM posts a
                LEFT JOIN posts q ON a.ParentId = q.Id
                INNER JOIN totalvotes ON a.Id == totalvotes.PostId
            WHERE a.PostTypeId == 2
            GROUP BY a.ParentId
        )
        GROUP BY ViewCount
        """
        viewcounts = []
        total_votes = []
        for r in self.execute(query):
            viewcounts.append(r[0])
            total_votes.append(r[1])
        return viewcounts, total_votes

    def get_score_vs_time(self):
        query = "SELECT TimeAfterQuestion, Score FROM extendedanswers"
        times = []
        score = []
        for r in self.execute(query):
            times.append(r[0])
            score.append(r[1])
        return score, times

    def get_age_vs_totalvotes(self):
        query = """
        SELECT
            a.TotalVotes,
            julianday(date('now')) - julianday(a2.CreationDate) AS Age,
            a2.CreationDate
        FROM
            extendedanswers a
        LEFT JOIN posts a2 ON a.PostId == a2.id
        """
        votes = []
        age = []
        for r in self.execute(query):
            votes.append(none2zero(r[0]))
            age.append(r[1])
        return age, votes

    def get_truths(self, min_votes = 0):
        query = "SELECT Score*1.0/(TotalVotes+1) FROM extendedanswers WHERE TotalVotes >= %s" % min_votes
        result = list(self.execute(query).fetchall())
        result = map(tuple2single, result)
        result = map(none2zero, result)
        return result




class DocumentSource(DatabaseConnection):
    def __init__(self, db_path, split=True, constrain_by=None):
        super(DocumentSource, self).__init__(db_path)
        query = "SELECT id FROM posts ORDER BY id DESC LIMIT 1"
        self.highest_post_id = list(self.execute(query))[0][0]
        self.split = split
        self.constrain_by = constrain_by

    def __iter__(self):
        related_questions_view = """
            CREATE VIEW IF NOT EXISTS relatedquestionsview AS
            SELECT
                PostId,
                group_concat(RelatedPostId) AS RelatedQuestions
            FROM postlinks
            WHERE RelatedPostId NOT IN (SELECT Id FROM posts WHERE PostTypeId = 2)
            GROUP BY PostId
        """
        get_documents = """
        SELECT
            Id,
            PostTypeId,
            Body,
            Title,
            Tags,
            RelatedQuestions
        FROM posts
        LEFT JOIN relatedquestionsview ON Id == PostId
        """
        if type(self.constrain_by) == str:
            get_documents += """
             WHERE ParentId IN (SELECT Id FROM knownquestions)
             OR Id IN (SELECT Id FROM knownquestions)
             """
        if type(self.constrain_by) == int:
            counter = 0
        self.execute(related_questions_view)
        self.execute(get_documents)
        for post_id, post_type_id, body, title, tags, related_questions in self.c:
            if type(self.constrain_by) == int:
                counter += 1
                if counter > self.constrain_by:
                    break
            tags = self.get_document_tags(post_id, post_type_id, tags, related_questions)
            yield self.output(title, tags)
            yield self.output(body, tags)

    def output(self, string, tags):
        from gensim.models.doc2vec import TaggedDocument
        if self.split:
            return TaggedDocument(words=self.get_wordlist(string), tags=tags)
        else:
            if string is None:
                return tuple("")
            else:
                return tuple(filter_out_rich_content(string))

    def get_wordlist(self, content):
        if content is None:
            return []
        else:
            return wordlist(content, filter_rich_content=True, filter_stopwords=True)

    def get_document_tags(self, post_id, post_type_id, stackexchange_tags, related_questions):
        tags = []
        tags.append(self.compose_post_tag(post_id, post_type_id))
        tags.append(self.compose_general_tag(post_type_id))
        tags.extend(self.compose_related_questions_taglist(related_questions))
        tags.extend(self.compose_stackexchange_taglist(stackexchange_tags))
        return tags

    def compose_related_questions_taglist(self, related_questions):
        if related_questions is None:
            return []
        else:
            tags = ["QUESTION_%s" % question_id for question_id in related_questions.split(",")]
            return list(set(tags))

    def compose_stackexchange_taglist(self, tags):
        if tags is None:
            return []
        else:
            return tags2list(tags)

    def compose_post_tag(self, post_id, post_type_id):
        if post_type_id == 1:
            return 'QUESTION_%s' % post_id
        else:
            return 'ANSWER_%s' % post_id

    def compose_general_tag(self, post_type_id):
        if post_type_id == 1:
            return "QUESTION"
        else:
            return "ANSWER"
