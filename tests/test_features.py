import unittest
import time

from stackexchangedata.db import *
from stackexchangedata.features.featuresets import *
from stackexchangedata.features.textfeatures import *

from utils.settings import get_db, db_types, get_settings
from utils.ngram import NgramModel

test_db = get_db("beer")
doc2vec = get_db("beer", "doc2vec")
unigram = get_db("beer", "unigram")
trigram = get_db("beer", "trigram")
settings = get_settings("beer")

def setUpModule():
    from scripts.extenddb import remove_extensions, extend_db
    remove_extensions(DatabaseConnection(test_db))
    extend_db(DatabaseConnection(test_db))


class TestHigherFeatures(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)
        self.answer = self.data.get_answer(2)
        self.answer2 = self.data.get_answer(44)
        self.answerer = AnswererFeaturesProvider(self.answer)
        self.asker = AskerFeaturesProvider(self.answer2.get_corresponding_question())
        self.t0= float(time.time())

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)

    def test_percent_of_total_votes_received(self):
        self.assertEqual(self.answer.get_relative_votes_received(), 0.84)

    def test_relative_votes_of_answerer(self):
        self.assertTrue(-1 <= self.answerer.get_relative_score() <= 1.0)

    def test_relative_answers_accepted_of_answerer(self):
        self.assertEqual(self.answerer.get_fraction_of_accepted_answers(), 0)

    def test_answerer_score(self):
        self.assertTrue(0 <= self.answerer.get_score() <= 21)

    def test_asker_total_answers(self):
        self.assertTrue(0 <= self.asker.get_total_answers_received() <= 1)

    def test_asker_received_answers_average_score(self):
        self.assertTrue(0 <= self.asker.get_received_answers_average_score() <= 2)

    def test_asker_received_answers_total_length(self):
        self.assertTrue(0 <= self.asker.get_received_answers_total_length() <= 360)

    def test_asker_average_answers_received_with_votes(self):
        self.assertTrue(0 <= self.asker.get_average_answers_received_with_votes() <= 1)


class TestTextFeatures(unittest.TestCase):
    def setUp(self):
        self.s = """
            <p>Let $F(x)$ denote the cdf; then you can always approximate the pdf of a continuous random variable
            by calculating $$ \frac{F(x_2) - F(x_1)}{x_2 - x_1},$$ where $x_1$ and $x_2$ are on either side of the
            point where you want to know the pdf and the distance $|x_2 - x_1|$ is small.</p>
            """
        self.tfp = TextFeaturesProvider(self.s)
        self.t0= float(time.time())

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)

    def test_nb_long_words(self):
        s = "This is a test string with one complicated word."
        self.assertEqual(TextFeaturesProvider(s).nb_long_words(), 1)
        self.assertEqual(self.tfp.nb_long_words(), 5)

    def test_relative_nb_long_words(self):
        s = " "
        self.assertEqual(TextFeaturesProvider(s).nb_long_words(relative=True), 0)
        s = "We have one extremely long word."
        self.assertEqual(TextFeaturesProvider(s).nb_long_words(relative=True), 1.0/6)
        self.assertEqual(self.tfp.nb_long_words(relative=True), 5.0/41)

    def test_answer_length(self):
        s = "This is a test."
        self.assertEqual(TextFeaturesProvider(s).length(), 15)

    def test_nb_unique_words(self):
        s1 = "This is a test."
        self.assertEqual(TextFeaturesProvider(s1).nb_unique_words(), 4)
        s2 = "This is a test ."
        self.assertEqual(TextFeaturesProvider(s2).nb_unique_words(), 4)
        s3 = "This is is a test ."
        self.assertEqual(TextFeaturesProvider(s3).nb_unique_words(), 4)
        self.assertEqual(self.tfp.nb_unique_words(), 32)

    def test_relative_nb_unique_words(self):
        s1 = "This is is a test ."
        self.assertEqual(TextFeaturesProvider(s1).nb_unique_words(relative=True), 4.0/5)
        self.assertEqual(self.tfp.nb_unique_words(relative=True), 32.0/41)

    def test_kincaid(self):
        s = "The Australian platypus is seemingly a hybrid of a mammal and reptilian creature."
        self.assertEqual(TextFeaturesProvider(s).kincaid(), 37.455384615384645)
        s = "No delimiter"
        self.assertLess(TextFeaturesProvider(s).kincaid(), 0)
        s = " "
        self.assertEqual(TextFeaturesProvider(s).kincaid(), 206.835)
        s = "          "
        self.assertEqual(TextFeaturesProvider(s).kincaid(), 206.835)
        s = ""
        self.assertEqual(TextFeaturesProvider(s).kincaid(), 206.835)
        self.assertEqual(self.tfp.kincaid(), 49.66878048780491)

    def test_nb_codeblocks(self):
        s1 = "This is a text with <code>print 'hello world'</code>"
        self.assertEqual(TextFeaturesProvider(s1).nb_code_blocks(), 1)
        self.assertEqual(TextFeaturesProvider(s1).nb_code_blocks(relative=True), 1.0/52)
        s2 = "This is a text without code"
        self.assertEqual(TextFeaturesProvider(s2).nb_code_blocks(), 0)
        self.assertEqual(TextFeaturesProvider(s2).nb_code_blocks(relative=True), 0)
        s3 = "This is a text with a <code></code> block and another one <pre><code></code></pre>"
        self.assertEqual(TextFeaturesProvider(s3).nb_code_blocks(), 2)
        self.assertEqual(TextFeaturesProvider(s3).nb_code_blocks(relative=True), 2.0/82)
        self.assertEqual(self.tfp.nb_code_blocks(), 0)

    def test_nb_images(self):
        s1 = "Text with an <img src='l.png' />"
        self.assertEqual(TextFeaturesProvider(s1).nb_images(), 1)
        self.assertEqual(TextFeaturesProvider(s1).nb_images(relative=True), 1.0/32)
        s2 = "This is a text without an image"
        self.assertEqual(TextFeaturesProvider(s2).nb_images(), 0)
        self.assertEqual(TextFeaturesProvider(s2).nb_images(relative=True), 0)
        self.assertEqual(self.tfp.nb_images(), 0)

    def test_nb_latex_blocks(self):
        strings = {}
        strings["Pythagoras: $a^2 + b^2 = c^2$"] = 1
        strings["Pythagoras: $$a^2 + b^2 = c^2$$"] = 1
        strings["Pythagoras: \[a^2 + b^2 = c^2\]"] = 1
        strings["Pythagoras: \(a^2 + b^2 = c^2\)"] = 1
        strings["Pythagoras was a philosopher with 1$"] = 0
        strings["Let $a, b, c$ be the sides of this, then $$a^2 + b^2 = c^2$$"] = 2
        strings[self.s] = 5
        expected = {1: 1, 2: 1, 3: 1, 4: 1, 5: 0, 6: 2, 7: 5}
        for s, nb_expected_blocks in strings.items():
            self.assertEqual(TextFeaturesProvider(s).nb_latex_blocks(), nb_expected_blocks)
            self.assertEqual(TextFeaturesProvider(s).nb_latex_blocks(relative=True), float(nb_expected_blocks)/len(s))

    def test_unigram_model_entropy(self):
        model = NgramModel.load_from_file(unigram)
        s = "We discuss beer."
        self.assertLess(TextFeaturesProvider(s).entropy(model), -1)

    def test_trigram_model_entropy(self):
        model = NgramModel.load_from_file(trigram)
        s = "We discuss beer."
        self.assertLess(TextFeaturesProvider(s).entropy(model), -1)

    def test_doc2vec_complete(self):
        from gensim.models import Doc2Vec
        model = Doc2Vec.load(doc2vec)
        answer = DataObjectSource(test_db).get_answer(2)
        r = TextFeaturesProvider(answer.body).doc2vec_complete(answer.id, model)
        self.assertEqual(type(r), tuple)
        print r
        self.assertEqual(len(r), settings["doc2vec_size"])


class TestTextComparisionFeaturesProvider(unittest.TestCase):
    def test_word_overlap(self):
        s1 = "The brown cat is fast"
        s2 = "The cat is red and fast"
        self.assertEqual(TextComparisionFeaturesProvider(s1, s2).word_overlap(), 4)
        s1 = "They may have eaten"
        s2 = "I have eaten"
        self.assertEqual(TextComparisionFeaturesProvider(s1, s2).word_overlap(), 2)

    def test_length_ratio(self):
        s1 = "A short string."
        s2 = "This is a much longer string."
        self.assertEqual(TextComparisionFeaturesProvider(s1, s2).length_ratio(), float(15)/29)

    def test_similarity_to_question(self):
        from gensim.models import Doc2Vec
        model = Doc2Vec.load(doc2vec)
        answer = DataObjectSource(test_db).get_answer(2)
        similarity = TextComparisionFeaturesProvider().similarity_to_question(answer.post_id, answer.parent_id, model)
        self.assertTrue(-1 <= similarity <= 1, msg="similarity = %s" % similarity)

    def test_colinearity_with_general_qa_pair(self):
        from gensim.models import Doc2Vec
        model = Doc2Vec.load(doc2vec)
        answer = DataObjectSource(test_db).get_answer(2)
        features_provider = TextComparisionFeaturesProvider()
        colinearity = features_provider.colinearity_with_general_qa_pair(answer.post_id, answer.parent_id, model)
        self.assertTrue(0 <= colinearity <= 1, msg="colinearity = %s" % colinearity)


class TestFeaturesets(unittest.TestCase):
    def setUp(self):
        self.db = DataObjectSource(test_db)
        answer_ids = list(self.db.execute("SELECT Id FROM Posts WHERE PostTypeId = 2 AND ParentId NOT IN (SELECT Id FROM knownquestions)"))
        answer_ids = [tuple2single(e) for e in answer_ids]
        self.answer_ids = answer_ids
        self.t0= float(time.time())
        self.featureset = None

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)

    def assertListAlmostEqual(self, list1, list2, tol=None, remark=""):
        self.assertEqual(len(list1), len(list2))
        for i, pair in enumerate(zip(list1, list2)):
            fail_message = "%s\nList entries %s are not equal: %s != %s. Complete lists:\n%s\n%s " % (remark, i, pair[0], pair[1], list1, list2)
            try:
                self.assertAlmostEqual(pair[0], pair[1], tol, msg=fail_message)
            except TypeError:
                AssertionError(fail_message)

    def assertNiceFeatures(self, features):
        self.assertIsInstance(features, tuple)
        self.assertNotIn(None, features)

    def test_from_extended_answer(self):
        extended_answer = self.db.get_extended_answer(self.answer_ids[0])
        features = self.featureset.from_extended_answer(extended_answer)
        print "from_extended_answer features: %s" % str(features)
        self.assertNiceFeatures(features)

    def test_from_answer(self):
        answer = self.db.get_answer(self.answer_ids[0])
        features = self.featureset.from_answer(answer)
        print "from_answer features: %s" % str(features)
        self.assertNiceFeatures(features)

    def test_from_answer_and_from_extended_answer_give_same_result(self):
        for answer_id in self.answer_ids:
            answer = self.db.get_answer(answer_id)
            extended_answer = self.db.get_extended_answer(answer_id)
            features_from_answer = self.featureset.from_answer(answer)
            features_from_extended_answer = self.featureset.from_extended_answer(extended_answer)
            self.assertListAlmostEqual(features_from_answer, features_from_extended_answer)

    def test_same_number_of_labels_and_values_in_from_answer(self):
        answer = self.db.get_answer(self.answer_ids[10])
        features = self.featureset.from_answer(answer)
        labels = self.featureset.labels()
        self.assertEqual(len(features), len(labels))

    def test_same_number_of_labels_and_values_in_from_extended_answer(self):
        extended_answer = self.db.get_extended_answer(self.answer_ids[10])
        features = self.featureset.from_extended_answer(extended_answer)
        self.assertEqual(len(features), len(self.featureset.labels()))

    def test_available_in_db_types(self):
        self.assertEqual(type(db_types[self.featureset.name()]), str)


class TestHalfOfAgichteinFeatures(TestFeaturesets):
    def setUp(self):
        super(TestHalfOfAgichteinFeatures, self).setUp()
        self.featureset = HalfOfAgichteinFeatures()


class TestAgichteinFeatures(TestFeaturesets):
    def setUp(self):
        super(TestAgichteinFeatures, self).setUp()
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = AgichteinFeatures(unigram_model, trigram_model)


class TestEPFL1Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL1Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        self.featureset = EPFL1Features(doc2vec_model)
        self.assertEqual(self.featureset.name(), "epfl1features")


class TestEPFL2Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL2Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        self.featureset = EPFL2Features(doc2vec_model)
        self.assertEqual(self.featureset.name(), "epfl2features")


class TestEPFL3Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL3Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        self.featureset = EPFL3Features(doc2vec_model)
        self.assertEqual(self.featureset.name(), "epfl3features")


class TestEPFL4Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL4Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = EPFL4Features(doc2vec_model, unigram_model, trigram_model)
        self.assertEqual(self.featureset.name(), "epfl4features")


class TestEPFL4FeaturesWithoutDoc2Vec(TestFeaturesets):
    def setUp(self):
        super(TestEPFL4FeaturesWithoutDoc2Vec, self).setUp()
        doc2vec_model = None
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = EPFL4Features(doc2vec_model, unigram_model, trigram_model)
        self.assertEqual(self.featureset.name(), "epfl4features")


class TestEPFL5Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL5Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = EPFL5Features(doc2vec_model, unigram_model, trigram_model)
        self.assertEqual(self.featureset.name(), "epfl5features")


class TestEPFL6Features(TestFeaturesets):
    def setUp(self):
        super(TestEPFL6Features, self).setUp()
        from gensim.models import Doc2Vec
        doc2vec_model = Doc2Vec.load(doc2vec)
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = EPFL6Features(doc2vec_model, unigram_model, trigram_model)
        self.assertEqual(self.featureset.name(), "epfl6features")


class TestEPFL6FeaturesWithoutDoc2Vec(TestFeaturesets):
    def setUp(self):
        super(TestEPFL6FeaturesWithoutDoc2Vec, self).setUp()
        doc2vec_model = None
        unigram_model = NgramModel.load_from_file(unigram)
        trigram_model = NgramModel.load_from_file(trigram)
        self.featureset = EPFL6Features(doc2vec_model, unigram_model, trigram_model)
        self.assertEqual(self.featureset.name(), "epfl6features")


class TestFeaturesetsCommonMethods(unittest.TestCase):
    def test_get_featuresets(self):
        for featureset_name in [db_type for db_type in db_types.keys() if "features" in db_type]:
            self.assertIsInstance(get_feature_set(featureset_name, "beer"), Features)



del TestFeaturesets
