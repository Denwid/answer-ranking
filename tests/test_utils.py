# coding=utf-8
import unittest
import collections

from utils.datetimeprocessing import *
from utils.generic import *
from utils.reporting import *
from utils.sqlite import *
from utils.languageprocessing import *
from utils.settings import *
from utils.stats import *


test_db = get_db("beer")


class TestDateTimeUtils(unittest.TestCase):
    def test_from_isotime(self):
        answer = from_isotime("2015-01-03T05:49:51.593")
        expected_answer = datetime.datetime(2015, 1, 3, 5, 49, 51, 593000)
        self.assertEqual(answer, expected_answer)

    def test_timedelta_in_minutes(self):
        self.assertEqual(timedelta_in_minutes("2015-01-03T05:49:51.593", "2015-01-03T05:50:51.593"), 1)
        self.assertEqual(timedelta_in_minutes("2015-01-03T05:49:51.593", "2015-01-03T06:50:51.593"), 61)
        self.assertEqual(timedelta_in_minutes("2015-01-03T05:49:51.593", "2015-01-03T05:49:54.593"), 0.05)
        self.assertEqual(timedelta_in_minutes("2015-01-03T05:49:51.593", "2015-01-04T05:49:51.593"), 1440)

    def test_timestamps2timedeltas(self):
        timestamp_list = ["2015-01-03T05:49:51.593", "2015-01-03T05:50:51.593", "2015-01-03T05:51:51.593"]
        answer = timestamps2timedeltas(timestamp_list)
        expected_answer = [1, 1]
        self.assertEqual(answer, expected_answer)

        timestamp_list = ["2015-01-03T05:49:51.593"]
        answer = timestamps2timedeltas(timestamp_list)
        expected_answer = []
        self.assertEqual(answer, expected_answer)


class TestGenericUtils(unittest.TestCase):
    def test_rank_list(self):
        l = [13, 29, 1, 8]
        expected_ranks = [2, 1, 4, 3]
        self.assertEqual(rank_list(l), expected_ranks)

    def test_none_or_float_2str(self):
        self.assertEqual(none_or_float2str(None), "None")
        self.assertEqual(none_or_float2str(3.12651), "3.13")
        self.assertEqual(none_or_float2str(3), "3.00")
        self.assertEqual(none_or_float2str(123.456), "123.46")

    def test_list_or_single2list(self):
        self.assertEqual(list_or_single2list(2), [2])
        self.assertEqual(list_or_single2list([2, 3]), [2, 3])
        self.assertEqual(list_or_single2list(None), [None])


class TestRankByMMR(unittest.TestCase):
    def setUp(self):
        self.docs = {1, -1, -0.8}
        self.q = -1
        self.similarity = lambda x, y: x*y

    def test_rank_by_mmr(self):
        s = mmr_sorted(self.docs, self.q, 0.3, self.similarity, self.similarity)
        self.assertEqual(list(s), [-1, 1, -0.8])

    def test_rank_by_mmr_lambda_1(self):
        s = mmr_sorted(self.docs, self.q, 1, self.similarity, self.similarity)
        self.assertEqual(list(s), [-1, -0.8, 1])

    def test_rank_by_mmr_lambda_0(self):
        s = mmr_sorted(self.docs, self.q, 0, self.similarity, self.similarity)
        self.assertEqual(list(s), [1, -1, -0.8])


class TestNormalizedCounter(unittest.TestCase):
    def test_init(self):
        NormalizableCounter([3, 3, 3])
        NormalizableCounter({3: 1}, 1)
        self.assertRaises(ValueError, NormalizableCounter, {3: 1})
        NormalizableCounter({}, 0)
        c = NormalizableCounter([])
        self.assertTrue(c.counted == 0)

    def test_normalize(self):
        self.assertEqual(dict(NormalizableCounter([3, 3, 3]).normalize()), {3: 1})
        self.assertEqual(dict(NormalizableCounter({}, 0).normalize()), {})

    def test_scale(self):
        self.assertEqual(dict(NormalizableCounter([3, 3, 3]).scale(2)), {3: 6})

    def test_add(self):
        c1 = NormalizableCounter([1, 2])
        c2 = NormalizableCounter([2, 3])
        self.assertEqual(dict(c1+c2), {1: 1, 2: 2, 3: 1})
        self.assertEqual((c1+c2).counted, 4)

    def test_inflate(self):
        c = NormalizableCounter({1: 1, 2: 2, 3: 1}, 4)
        c = c.normalize()
        l = c.inflate()
        self.assertEqual(l, [1, 2, 2, 3])

    def test_weighted_sum(self):
        c1 = NormalizableCounter([1, 2]).normalize().scale(0.25)
        c2 = NormalizableCounter([3]).normalize().scale(0.75)
        c_empty = NormalizableCounter({}, 0)
        self.assertEqual(dict(c1+c2), {1: 0.125, 2: 0.125, 3: 0.75})
        self.assertEqual((c1+c2).inflate(), [3, 3])
        self.assertEqual(dict(c_empty+c1+c2), {1: 0.125, 2: 0.125, 3: 0.75})


class TestLanguageprocessing(unittest.TestCase):
    def test_filter_out_latex(self):
        s1 = "Pythagoras: $a^2 + b^2 = c^2$"
        s2 = "Pythagoras: $$a^2 + b^2 = c^2$$"
        s3 = "Pythagoras: \[a^2 + b^2 = c^2\]"
        s4 = "Pythagoras: \(a^2 + b^2 = c^2\)"
        s5 = "Pythagoras was a philosopher with 1$"
        expected = "Pythagoras: "
        self.assertEqual(expected, filter_out_latex(s1))
        self.assertEqual(expected, filter_out_latex(s2))
        self.assertEqual(expected, filter_out_latex(s3))
        self.assertEqual(expected, filter_out_latex(s4))
        self.assertEqual(s5, filter_out_latex(s5))
        self.assertEqual(expected + expected + expected, filter_out_latex(s1+s2+s3))

    def test_filter_out_code(self):
        s1 = "This is <code>printf ('hello world')</code>"
        s2 = "A normal sentence."
        self.assertEqual("This is ", filter_out_code(s1))
        self.assertEqual(s2, filter_out_code(s2))

    def test_filter_out_html_tags(self):
        s1 = "<p>A paragraph with <span id='text'>elements </span>"
        self.assertEqual("A paragraph with elements ", filter_out_html_tags(s1))

    def test_filter_out_rich_content(self):
        s = """
            <p>Let $F(x)$ denote the cdf; then you can always approximate the pdf of a continuous random variable
            by calculating $$ \frac{F(x_2) - F(x_1)}{x_2 - x_1},$$ where $x_1$ and $x_2$ are on either side of the
            point where you want to know the pdf and the distance $|x_2 - x_1|$ is small.</p><code> d = abs(x1 - x2)</code>
            """
        expected = """
            Let  denote the cdf; then you can always approximate the pdf of a continuous random variable
            by calculating  where  and  are on either side of the
            point where you want to know the pdf and the distance  is small.
            """
        self.assertEqual(expected, filter_out_rich_content(s))

    def test_post2wordlist(self):
        s = ""
        self.assertEqual(wordlist(s), [])

    def test_tags2list(self):
        s1 = "<anova><chi-squared><generalized-linear-model>"
        l1 = ['anova', 'chi-squared', 'generalized-linear-model']
        s2 = ""
        l2 = []
        s3 = "<scales><measurement><ordinal><interval><likert>"
        l3 = ['scales', 'measurement', 'ordinal', 'interval', 'likert']
        self.assertEqual(tags2list(s1), l1)
        self.assertEqual(tags2list(s2), l2)
        self.assertEqual(tags2list(s3), l3)


class TestMath(unittest.TestCase):
    def test_mean_and_std(self):
        expected = (2, 1)
        truth = mean_and_std([1, 3])
        self.assertEqual(truth, expected)
        expected2 = (4, 4)
        truth2 = mean_and_std([0, 8])
        self.assertEqual(truth2, expected2)

    def test_mean(self):
        self.assertEqual(mean([0,1]), 0.5)
        self.assertEqual(mean([]), 0)
        self.assertEqual(mean([1,2,3,4,5]), 3)

    def test_norm_list(self):
        self.assertEqual(norm_list([1, 3]), [0.25, 0.75])
        self.assertEqual(norm_list([0.07, 0.14, 0.07]), [0.25, 0.50, 0.25])
        self.assertEqual(norm_list([]), [])

    def test_norm_dict(self):
        self.assertEqual(norm_dict({'a':0.2, 'b':0.3}), {'a': 0.4, 'b': 0.6})


class TestReporting(unittest.TestCase):
    def test_plot_histogram(self):
        import matplotlib.pyplot as plt
        self.assertIsInstance(plot_histogram("Test", [1, 1, 2, 3, 3], show=False), plt.Figure)

    def test_plot_log_histogram(self):
        import matplotlib.pyplot as plt
        self.assertIsInstance(plot_log_histogram("Test", numpy.array([1, 1, 2, 3, 3]), show=False), plt.Figure)

    def test_plot_semilogy_histogram(self):
        import matplotlib.pyplot as plt
        self.assertIsInstance(plot_semilogy_histogram("Test", [1, 1, 2, 3, 3], show=False), plt.Figure)

    def test_plot_text(self):
        import matplotlib.pyplot as plt
        title = "Test title"
        lines = ["This is a line", "and another one", ur'one with some $\tau$']
        self.assertIsInstance(plot_text(title, lines, show=False), plt.Figure)

    def test_coeff_bars(self):
        values = [ -5.78234544e-07,   2.55776413e-04,   5.28467696e-01,   1.62334030e-01,
                   4.16249423e-04,   1.08522461e-06,  5.83448584e-01, -6.40815808e-04, -1.23116929e-04]
        labels = ['A-len', 'A-words', 'A-rel.score', 'AU-%accecpted Answers', 'A-unique words',
                  'AU-reputation', 'AU-reports', 'AvQ-word overlap', 'A-kincaid']
        self.assertIsInstance(plot_hbars("Test bars", values, labels, show=False), plt.Figure)

    def test_plot_normalized_histogram(self):
        data = [0, 0.5, 0.5, 1, -1, -0.5, 0.25]
        data2 = [0.2, 0.1, 0.5, 1, -0.5, -0.3, 0.2]
        self.assertIsInstance(plot_normalized_histogram("Test Title", data, data2, show=False), plt.Figure)

    def test_plot_counter(self):
        data = collections.Counter([2, 3, 3])
        self.assertIsInstance(plot_counter("Test counter plot", data, show=False), plt.Figure)

    def test_plot_multibar(self):
        data = {
            "group_1": {"mean": 2, "std": 3, "std2": 4, "std3": 5},
            "group_2": {"mean": 2.5, "std": 2, "lala": 1.4},
            "group_3": {"yolo": 2.3}
        }
        self.assertIsInstance(plot_multibar("Multiple groups of values", data, show=False), plt.Figure)

    def test_plot_multibar_with_error_bars(self):
        data = {
            "group_1": {"model1": (12, 2), "model2": (10, 3)}
        }
        self.assertIsInstance(plot_multibar("Test model comparision", data, show=False), plt.Figure)

    def test_plot_boxplot(self):
        data = { "series1": [1, 2, 3, 2, 2.5], "series2": [2, 3, 4, 2.5] }
        self.assertIsInstance(plot_boxplot("Test boxplot", data, show=False), plt.Figure)

    def test_plot_scatter(self):
        x = [2, 3, 3, 2]
        y = [2, 3, 4, 1]
        self.assertIsInstance(plot_scatter("x vs. y", x, y, show=False), plt.Figure)


class TestSQLite(unittest.TestCase):
    def setUp(self):
        self.db = DatabaseConnection(test_db)

    def test_get_parent_dir(self):
        self.assertEqual(self.db.parent_dir, "fixtures")

    def test_execute_and_describe(self):
        query = "SELECT Id, PostTypeId, OwnerUserId FROM posts WHERE Id = 2"
        rows, description = self.db.execute_and_describe(query)
        self.assertEqual(tuple(list(rows)[0]), (2, 2, 32))
        self.assertEqual(description, ['Id', 'PostTypeId', 'OwnerUserId'])

    def test_row_and_description2dict(self):
        row = (2, 2, 32)
        description = ['Id', 'PostTypeId', 'OwnerUserId']
        result = row_and_description2dict(row, description)
        expected = {'Id': 2, 'PostTypeId': 2, 'OwnerUserId': 32}
        self.assertEqual(result, expected)