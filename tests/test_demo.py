import unittest

from stackexchangedata.db import DataObjectSource
from demo.htmlcomposer import RankingHtmlComposer
from utils.settings import get_db


test_db = get_db("beer")


def setUpModule():
    from utils.sqlite import DatabaseConnection
    from scripts.extenddb import remove_extensions, extend_db
    remove_extensions(DatabaseConnection(test_db))
    extend_db(DatabaseConnection(test_db))


class TestRankingHtmlComposer(unittest.TestCase):
    def setUp(self):
        self.db = DataObjectSource(test_db)
        self.composer = RankingHtmlComposer(1, self.db)

    def test_compose_ranking_html(self):
        html = self.composer.compose_ranking_html()
        self.assertEqual(type(html), unicode)