README - Ranking Answers on Stack Exchange
------------------------------------------

This directory contains code from my Master's Thesis "Ranking Answers on Stack Exchange"
The corresponding blog is maintained on http://masterthesis.meierfam.ch/
The code is publicly available on https://bitbucket.org/Denwid/answer-ranking/src

Overview
--------

The goal of this code is to provide means to automatically rank answers and then evaluate
said rankings to compare them. It provides a framework that allows computation of features
for answer quality learning, as well as interfaces to learning algorithms.

The scripts/ directory is the main entry point for users. It contains scripts to
perform all necessary tasks related to the experiments done for the project. Once the
necessary Python Packages are installed, in a typical session one would:
    1.  Download a data dump for a specific site (e.g. math.stackexchange.com) from
        https://archive.org/details/stackexchange
    2.  Extract this to some directory, add that path to the data_dirs dict in utils/settings.py
    3.  Call scripts/makedb.py on that newly added data dir
    4.  Call scripts/makedoc2vec.py and scripts/makengram.py to create language models
    5.  Call scripts/makefeatures.py to create a features database
    6.  Call scripts/makereport.py to train and evaluate a certain ranking method


Settings
--------
Next to the directory names of directories that contain data, utils/settings.py also contains
a settings dict that can be used to adjust certain parameters of the experiments.

- "doc2vec_training_epochs": How many times each answer will be fed to makedoc2vec.py when
creating doc2vec language models on the given data.
- "doc2vec_size": The dimnesionality of the vectors doc2vec will learn
- "features_featuresets": a Python list of featuresets that will be produced when
when calling scripts//makefeatures.py. Each featureset is a string and must be
available in the stackexchangedata/features/featuresets.py get_feature_set() function.
- "report_model": a Python list of models that can be used to predict answer quality,
each model is a string and must be available in the models/model.py models dict.
Relevant when calling scripts/makereport.py
- "report_min_answers": The minimum number of answers a question must have to be considered
in the report that is generated when calling scripts/makereport.py
- "report_create_pdf": set to True if you want scripts/makereport.py to output a .pdf report
next to the .json reports it generates anyways.
- "ngram_model_size": a Python list of ints. For each int a corresponing n-gram model will
be produced when calling scripts/makengram.py
- "train_by_score", "train_on_questions_with_one_answer_only" and "train_on_selected" are
hacks to enable training on score directly (as opposed to on ground-truth) and training
on these questions that have only one answer.


Directory Content
-----------------

demo/       Contains a little http-server that can connect to a features database display an
            imitated version of the corresponding Stack Exchange sites.

fixtures/   Contains the meta beer Stack Exchange which is used as a test dataset. Used by tests/

models/     Contains interfaces and objects representing machine learning models and methods
            to evaluate them.

scripts/    Provides the scripts that can be used to run the different experiments and tasks.
            This is the entry point for users that want to run something

stackexchangedata/  Provides a framework to connect to an SQLite database as imported from the
                    public Stack Exchange data dump with the scripts/makedb.py script.
                    This is the core part of the project, being used by the other parts.

tests/      Contains unittests for the different submodules

utils/      Contains small, shared code that is useful in many different parts of the project.

