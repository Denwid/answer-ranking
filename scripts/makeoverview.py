#!/usr/bin/python
import os
import sys
import inspect


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from models.evaluate import Overviewer
from utils.settings import get_db


data_sets = [
"server-test-cooking",
"server-test-electronics",
"server-test-math",
"server-test-physics",
"server-test-stats",
"server-test-english",
]
getdir = lambda x: get_db(x, type="dir")
data_dirs = [getdir(ds) for ds in data_sets]

output_path = "/home/dmeier/data/overview.pdf"

if not os.path.isfile(output_path):
    o = Overviewer(data_dirs)
    o.create_overview_pdf(output_path)
    print "Saved overview pdf to %s." % output_path
else:
    print "%s already exists, not executing makeoverview.py" % output_path
