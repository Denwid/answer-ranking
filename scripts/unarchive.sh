#!/bin/bash

ARCHIVE_DIR="/home/dmeier/data/test_zips"
TARGET_DIR="/home/dmeier/data/test_data"

for FILE in `ls $ARCHIVE_DIR`
do
	DATASET_NAME="${FILE%.*}"
	echo "=================== Processing $DATASET_NAME ====================="
	SOURCE=$ARCHIVE_DIR/$FILE
	TARGET="$TARGET_DIR/$DATASET_NAME"
	if [ -d "$TARGET" ]; then
		echo "Skipping $DATASET_NAME since $TARGET already exists"
		continue		
	fi
	mkdir $TARGET
	7z e $SOURCE -o$TARGET
done
