import time


def posts_index():
    return """
    CREATE INDEX posts_idx ON posts(Id)
    """

def knownquestions_table(known_fraction):
    return """
    CREATE TABLE knownquestions AS
    SELECT Id FROM posts WHERE PostTypeId == 1 AND random() > %s;
    """ % known_fraction


def posts_total_votes_view():
    return """
    CREATE VIEW totalvotes AS
    SELECT  PostId,
            count(CASE WHEN VoteTypeId=2 OR VoteTypeId=3 THEN 1 END) AS TotalVotes,
            count(CASE WHEN VoteTypeId=4 OR VoteTypeId=12 THEN 1 END) AS TotalReports,
            count(CASE WHEN VoteTypeId=1 THEN 1 END) AS Accepted
    FROM votes
    GROUP BY PostId
    """


def posts_totalcomments_view():
    return """
    CREATE VIEW totalcomments AS
    SELECT  PostId,
            count(*) AS Comments,
            avg(Score) AS AverageCommentScore
    FROM comments
    GROUP BY PostId
    """


def extended_posts_table():
    return """
    CREATE TABLE extendedposts AS
    SELECT  posts.Id AS PostId,
            posts.ParentId,
            posts.Score,
            posts.PostTypeId,
            posts.ViewCount,
            posts.Body,
            posts.OwnerUserId,
            totalvotes.TotalVotes,
            totalvotes.TotalReports,
            totalvotes.Accepted,
            totalcomments.Comments,
            totalcomments.AverageCommentScore
    FROM posts
        LEFT JOIN totalvotes ON posts.Id == totalvotes.PostId
        LEFT JOIN totalcomments ON posts.Id == totalcomments.PostId
    """


def answerers_view(question_selector):
    return """
    CREATE TABLE answerers AS
    SELECT  OwnerUserId AS UserId,
            count(*) AS AnswersWritten,
            sum(answer.TotalVotes) AS VotesReceived,
            sum(answer.Score) AS ScoreReceived,
            sum(answer.TotalReports) AS ReportsReceived,
            sum(answer.TotalReports)/count(*) AS ReportsReceivedPerAnswer,
            sum(answer.Score)*1.0/(sum(answer.TotalVotes)+1) AS RelativeScoreReceived,
            avg(answer.Accepted) AS FractionOfAcceptedAnswers,
            sum(answer.Accepted) AS AnswersAccepted
    FROM extendedposts answer
    WHERE   PostTypeId = 2 AND
            answer.ParentId IN (SELECT Id FROM %s)
    GROUP BY answer.OwnerUserId
    """ % question_selector


def extendedposts_postid_index():
    return """
    CREATE INDEX extendedposts_postid_idx ON extendedposts(PostId)
    """


def extendedposts_parentid_index():
    return """
    CREATE INDEX extendedposts_parentid_idx ON extendedposts(ParentId)
    """


def extendedposts_owneruserid_index():
    return """
    CREATE INDEX extendedposts_owneruserid_idx ON extendedposts(OwnerUserId)
    """


def relation_view():
    return """
    CREATE TABLE relations AS
    SELECT  answer.Id AS AnswerId,
            question.Id AS QuestionId,
            julianday(answer.CreationDate) - julianday(question.CreationDate) as TimeAfterQuestion
    FROM posts answer
        INNER JOIN posts question ON answer.ParentId == question.Id
    WHERE answer.PostTypeId = 2
    """


def relations_answerid_index():
    return """
    CREATE INDEX relations_answerid_idx ON relations(AnswerId)
    """


def question_view():
    return """
    CREATE TABLE questions AS
    SELECT  question.PostId AS QuestionId,
            question.OwnerUserId AS AskerId,
            question.Body AS QuestionBody,
            question.TotalReports AS Reports,
            count(*) AS Answers,
            avg(answer.Score) AS AnswersAverageScore,
            sum(length(answer.Body)) AS AnswersTotalLength,
            sum(answer.TotalVotes > 0) AS AnswersWithVotes
    FROM extendedposts question
        INNER JOIN extendedposts answer ON question.PostId == answer.ParentId
    WHERE question.PostTypeId = 1
    GROUP BY QuestionId
    """


def questions_askerid_index():
    return """
    CREATE INDEX questions_askerid_idx ON questions(AskerId);
    """


def question_id_index():
    return """
    CREATE INDEX questions_questionid_idx ON questions(QuestionId);
    """


def askers_view(question_selector):
    return """
    CREATE TABLE askers AS
    SELECT  questions.AskerId AS UserId,
            sum(questions.Answers) AS AnswersReceived,
            sum(questions.Answers)/count(*) AS AnswersReceivedPerQuestion,
            sum(questions.AnswersAverageScore*questions.Answers)*1.0/sum(questions.Answers) AS AnswersReceivedAverageScore,
            sum(questions.AnswersTotalLength) AS AnswersReceivedTotalLength,
            sum(questions.AnswersWithVotes)/count(*) AS AverageAnswersReceivedWithVotes,
            sum(questions.Reports) AS ReportsReceived
    FROM questions
    WHERE questions.QuestionId IN (SELECT Id FROM %s)
    GROUP BY UserId
    """ % question_selector


def extended_answers_table():
    return """
    CREATE TABLE extendedanswers AS
    SELECT
        answer.PostId,
        answer.ParentId,
        answer.Score,
        answer.TotalVotes,
        answer.ViewCount,
        answer.Body,
        answer.OwnerUserId,
        answer.PostTypeId,
        answer.TotalReports,
        answer.Comments,
        answer.AverageCommentScore,
        questions.QuestionBody,
        relations.TimeAfterQuestion,
        answerers.UserId AS AnswererUserId,
        answerers.AnswersWritten AS AnswererAnswersWritten,
        answerers.VotesReceived AS AnswererVotesReceived,
        answerers.ScoreReceived AS AnswererScoreReceived,
        answerers.ReportsReceived AS AnswererReportsReceived,
        answerers.ReportsReceivedPerAnswer AS AnswererReportsReceivedPerAnswer,
        answerers.RelativeScoreReceived AS AnswererRelativeScoreReceived,
        answerers.FractionOfAcceptedAnswers AS AnswererFractionOfAcceptedAnswers,
        askers.UserId AS AskerUserId,
        askers.AnswersReceived AS AskerAnswersReceived,
        askers.AnswersReceivedPerQuestion AS AskerAnswersReceivedPerQuestion,
        askers.AnswersReceivedAverageScore AS AskerAnswersReceivedAverageScore,
        askers.AnswersReceivedTotalLength AS AskerAnswersReceivedTotalLength,
        askers.AverageAnswersReceivedWithVotes AS AskerAverageAnswersReceivedWithVotes,
        askers.ReportsReceived AS AskerReportsReceived
    FROM extendedposts answer
        LEFT JOIN questions ON answer.ParentId == questions.QuestionId
        LEFT JOIN answerers ON answer.OwnerUserId = answerers.UserId
        LEFT JOIN askers ON questions.AskerId = askers.UserId
        LEFT JOIN relations ON answer.PostId = relations.AnswerId
    WHERE answer.ParentId NOT IN (SELECT Id FROM knownquestions)
    """


def extend_db(db, known_fraction=0.5):
    commands = [
        posts_index(),
        knownquestions_table(known_fraction),
        posts_total_votes_view(),
        posts_totalcomments_view(),
        extended_posts_table(),
        extendedposts_postid_index(),
        extendedposts_parentid_index(),
        relation_view(),
        relations_answerid_index(),
        question_view(),
        questions_askerid_index(),
        question_id_index(),
        answerers_view("knownquestions"),
        askers_view("knownquestions"),
        extended_answers_table()
    ]

    s = 0
    remove_extensions(db)
    for c in commands:
        first_line_of_command = c.split('\n', 2)[1]
        t0= float(time.time())
        db.execute(c)
        t = float(time.time()) - t0
        print "%s: %.3f" % (first_line_of_command, t)
        s += t
    print "total time: %.3f" % s


def remove_extensions(db):
    db.execute("DROP INDEX IF EXISTS extendedposts_postid_idx")
    db.execute("DROP INDEX IF EXISTS extendedposts_parentid_idx")
    db.execute("DROP INDEX IF EXISTS posts_idx")
    db.execute("DROP TABLE IF EXISTS knownquestions")
    db.execute("DROP TABLE IF EXISTS trainquestions")
    db.execute("DROP TABLE IF EXISTS testquestions")
    db.execute("DROP VIEW IF EXISTS totalvotes")
    db.execute("DROP VIEW IF EXISTS totalcomments")
    db.execute("DROP TABLE IF EXISTS extendedposts")
    db.execute("DROP VIEW IF EXISTS useracceptance")
    db.execute("DROP TABLE IF EXISTS answerers")
    db.execute("DROP TABLE IF EXISTS relations")
    db.execute("DROP INDEX IF EXISTS relations_answerid_idx")
    db.execute("DROP TABLE IF EXISTS questions")
    db.execute("DROP INDEX IF EXISTS questions_askerid_idx")
    db.execute("DROP INDEX IF EXISTS questions_questionid_idx")
    db.execute("DROP TABLE IF EXISTS answers")
    db.execute("DROP TABLE IF EXISTS askers")
    db.execute("DROP TABLE IF EXISTS extendedanswers")