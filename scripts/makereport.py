#!/usr/bin/python
import os
import sys
import inspect
import logging


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from models.featuresource import FeatureSource
from models.model import Model
from models.evaluate import ModelEvaluator
from stackexchangedata.db import InfoSource
from utils.settings import get_db_name_from_input, get_db, get_db_logfile, get_settings
from utils.files import any_file_exists


dataset_name = get_db_name_from_input()
db = get_db(dataset_name)
settings = get_settings(dataset_name)
counter = 1
total = len(settings["report_features_db"])*len(settings["report_model"])
for feature_type in settings["report_features_db"]:
    features_db = get_db(dataset_name, feature_type)
    logfile = get_db_logfile(dataset_name)
    logging.basicConfig(filename=logfile,level=logging.INFO)

    info_source = InfoSource(db)
    if settings["train_on_questions_with_one_answer_only"] is not None:
        min_votes = settings["train_on_questions_with_one_answer_only"]
        selected_questions = info_source.get_ids_of_questions_with_one_voted_answer(min_votes)
    else:
        selected_questions = None

    feature_source = FeatureSource(features_db, selected_questions)

    for model_type in settings["report_model"]:
        if settings["train_by_score"]:
            additional_tag = "_score"
        elif settings["train_on_questions_with_one_answer_only"]:
            additional_tag = "_single"
        else:
            additional_tag = ""
        print "Creating report %s/%s (dataset: %s, features: %s, model: %s%s)..." % (dataset_name, counter, total, feature_type, model_type, additional_tag)
        counter += 1
        model = Model(model_type)
        if dataset_name not in ["beer"]:
            model.make_fancy()
        evaluator = ModelEvaluator(model, info_source, feature_source)

        if not any_file_exists(evaluator.list_train_and_test_files(additional_tag=additional_tag)):
            model.train(feature_source, settings["train_by_score"], train_on_selected=bool(selected_questions))
            min_answers = settings["report_min_answers"]
            create_pdf = settings["report_create_pdf"]
            evaluator.create_train_and_test_reports(min_answers, additional_tag=additional_tag, create_pdf=create_pdf)
        else:
            print "At least one of the following reports already exists, not executing makereport.py:"
            for f in evaluator.list_train_and_test_files():
                print f
