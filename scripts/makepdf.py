#!/usr/bin/python
import os
import sys
import inspect
import json
import glob


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from models.evaluate import ReportContent

from utils.settings import get_db_name_from_input, get_db, get_settings
from utils.files import any_file_exists
from utils.reporting import save_as_pdf

dataset_name = get_db_name_from_input()
settings = get_settings(dataset_name)

dataset_dir = get_db(dataset_name, "dir")
json_reports = glob.glob(os.path.join(dataset_dir, "*.json"))
for report in json_reports:
    print "Processing %s" % report
    path = os.path.splitext(report)[0]+".pdf"
    if not any_file_exists([path]):
        print "Creating %s" % path
        r = json.load(open(report))
        r = ReportContent(r)
        figures = r.compose_figures()
        save_as_pdf(figures, path)
    else:
        print "%s already exists, leaving it as is." % path