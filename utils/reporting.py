import matplotlib as mpl
import matplotlib.pyplot as plt
import sklearn.tree

import numpy as np
import json

from matplotlib.backends.backend_pdf import PdfPages

from utils.stats import min_max


def set_plot_fontsize(size=16):
    mpl.rcParams.update({'font.size': size})
set_plot_fontsize()


def set_plot_font(name='Palatino Linotype'):
    mpl.rcParams.update({'font.family': name})
set_plot_font()


def pimp_title(title, data):
    if type(data) == list:
        return title
    mean = np.mean(data)
    std = np.std(data)
    return title + " (mean = %1.2f, std = %1.2f)" % (mean, std)


def plot_histogram(title, data, show=True, **kwargs):
    fig = plt.figure()
    if data != []:
        plt.hist(data, **kwargs)
    if type(data) == list:
        plt.legend()
    plt.title(pimp_title(title, data))
    plt.xlabel("Values")
    plt.ylabel("Frequency")
    if show:
        plt.show()
    return fig


def plot_normalized_histogram(title, data, background_data=None, nb_bins=50, show=True, maxrange=None, cumulative=False, log=False):
    fig = plt.figure()
    if data != []:
        plt.hist(data, normed=True, bins=nb_bins, range=maxrange, facecolor='blue', cumulative=cumulative)
    if background_data is not None and background_data != []:
        plt.hist(background_data, normed=True, bins=nb_bins, range=maxrange, facecolor='gray', alpha=0.5, cumulative=cumulative, log=log)
    plt.xlim(maxrange)
    plt.title(pimp_title(title, data))
    plt.xlabel("Values")
    plt.ylabel("Density")
    if show:
        plt.show()
    return fig


def plot_counter(title, normalized_counter, show=True):
    fig = plt.figure()
    values = normalized_counter.keys()
    relative_freq = normalized_counter.values()
    plt.bar(values, relative_freq)
    if show:
        plt.show()
    return fig


def plot_log_histogram(title, data, bins=50, show=True, xlim=None, set_fill=False, **kwargs):
    fig = plt.figure()
    if xlim:
        minimum = xlim[0]
        maximum = xlim[1]
    else:
        minimum, maximum = min_max(data)
    _, _, patches = plt.hist(data, bins=10**np.linspace(np.log10(minimum), np.log10(maximum), num=bins), **kwargs)
    plt.title(pimp_title(title, data))
    plt.xlabel("Values")
    plt.ylabel("Frequency")
    plt.gca().set_xscale("log")
    plt.gca().set_yscale("log")
    if type(data) == list:
        plt.legend()
    else:
        for p in patches:
            p.set_linewidth(2)
            p.set_fill(set_fill)
            p.set_edgecolor('black')
    if show:
        plt.show()
    return fig


def plot_semilogy_histogram(title, data, nb_bins=50, show=True, maxrange=None, normed=False):
    fig = plt.figure()
    _, _, patches = plt.hist(data, bins=nb_bins, range=maxrange, histtype='bar', normed=normed)
    plt.title(pimp_title(title, data))
    plt.xlabel("Values")
    plt.ylabel("Frequency")
    plt.gca().set_yscale("log")
    for p in patches:
        p.set_linewidth(2)
        p.set_fill(False)
        p.set_edgecolor('black')
    if show:
        plt.show()
    return fig


def plot_text(title, text_lines, show=True):
    fig = plt.figure()
    plt.title(title)
    ax = fig.add_subplot(111)
    ax.axis([0, 10, 0, 10])
    plt.axis('off')
    for i, line in enumerate(text_lines):
        ax.text(0.0, 10-(i/1.5+0.5), line, fontsize=10)
    if show:
        plt.show()
    return fig


def plot_hbars(title, values, labels=None, show=True, height=0.4):
    if labels is None:
        labels = range(len(values))
    y_pos = np.arange(len(labels))
    fig = plt.figure()
    plt.title(title)
    plt.barh(y_pos+height/2, values, color='g', alpha=0.7, height=height)
    plt.yticks(y_pos+height, labels)
    fig.tight_layout()
    if show:
        plt.show()
    return fig


def plot_multibar(title, data_groups, show=True, xlim=None, show_xerr=True):
    """
    Plot groups of bars for different "data groups"
    Inspired by http://stackoverflow.com/questions/14270391/python-matplotlib-multiple-bars

    :param data_groups: a dictionary of dictionaries: group_name->(label->value)
    :return: a pyplot Figure object with the corresponding multibar plot
    """
    fig = plt.figure()
    plt.title(title)

    max_labels_in_a_group = max([len(d.keys()) for d in data_groups.values()])
    if max_labels_in_a_group == 0:
        return fig
    slot_height = 1.0/max_labels_in_a_group
    height = 0.7*slot_height

    y_ticks = []
    y_labels = []
    for i, g in enumerate(data_groups):
        group_base_y = i
        for j, (label, value) in enumerate(data_groups[g].items()):
            y_pos = group_base_y + j*slot_height
            x_value = value[0] if type(value) == tuple else value
            x_std = value[1] if type(value) == tuple and show_xerr else None
            if x_value is not None:
                plt.barh(y_pos, x_value, color="yellow", height=height, align="center", xerr=x_std, error_kw=dict(ecolor='black'))
            y_ticks.append(y_pos)
            y_labels.append(g+"."+label)

    plt.yticks(y_ticks, y_labels)
    plt.xlim(xlim)
    fig.tight_layout()
    if show:
        plt.show()
    return fig


def plot_boxplot(title, data_series, show=True, xlim=None):
    fig = plt.figure()
    plt.title(title)
    labels = data_series.keys()
    if labels == []:
        return fig
    data = np.array([np.array(s) for s in data_series.values()]).transpose()
    plt.boxplot(data, labels=labels, vert=False)
    plt.xlim(xlim)
    fig.tight_layout()
    if show:
        plt.show()
    return fig


def plot_scatter(title, x, y, x2=None, y2=None, xlim=None, ylim=None, show=True, logx=False, logy=False):
    fig = plt.figure()
    plt.title(title)
    plt.scatter(x, y)
    if x2 is not None and y2 is not None:
        plt.scatter(x2, y2, c='r')
    plt.xlabel(title.split(" vs. ")[0])
    plt.ylabel(title.split(" vs. ")[1])
    plt.xlim(xlim)
    plt.ylim(ylim)
    if logx:
        plt.gca().set_xscale("log")
    if logy:
        plt.gca().set_yscale("log")
    fig.tight_layout()
    if show:
        plt.show()
    return fig


def plot_hist2d(title, x, y, bins=100, range=None, show=False):
    from matplotlib.colors import LogNorm
    fig = plt.figure()
    plt.title(title)
    plt.hist2d(x, y, bins=bins, norm=LogNorm(), range=range)
    if show:
        plt.show()
    return fig


def plot_binned_statistic(title, x, y, bins=100, xmin=None, xmax=None, logx=False, show=False, ylim=None, statistic='mean'):
    xmin = min(x) if xmin is None else xmin
    xmax = max(x) if xmax is None else xmax
    if logx:
        bins = np.logspace(xmin, xmax, num=bins)
    else:
        bins = np.linspace(xmin, xmax, num=bins)
    from scipy import stats
    bin_means, bin_edges, _ = stats.binned_statistic(x, y, statistic=statistic, bins=bins)
    fig = plt.figure()
    plt.title(title)
    plt.hlines(bin_means, bin_edges[:-1], bin_edges[1:], linewidth=2)
    plt.xlabel(title.split(" vs. ")[0])
    plt.ylabel(title.split(" vs. ")[1])
    plt.ylim(ylim)
    if logx:
        plt.gca().set_xscale('log')
    if show:
        plt.show()
    return fig

def save_as_pdf(figures, path):
    pages = PdfPages(path)
    for f in figures:
        pages.savefig(f)
    pages.close()


def save_as_json(dictionary, path):
    with open(path, 'w') as fp:
        json.dump(dictionary, fp)


def load_json(path):
    with open(path, 'r') as fp:
        return json.load(fp)


def save_dot(decision_tree_model, path):
    if isinstance(decision_tree_model, sklearn.tree.DecisionTreeRegressor):
        sklearn.tree.export_graphviz(decision_tree_model, out_file=path)