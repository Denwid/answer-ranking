import os, errno


def silentremove(filename):
    """ Remove file if exists. Taken from
    http://stackoverflow.com/questions/10840533/most-pythonic-way-to-delete-a-file-which-may-not-exist"""
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured


def any_file_exists(list_of_files):
    for f in list_of_files:
        if os.path.isfile(f):
            return True
    return False