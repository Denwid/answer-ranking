import sqlite3


def tuple2single(tuple):
    return tuple[0]


def single2tuple(single):
    return (single,)


def row_and_description2dict(row, description):
    return dict(zip(description, row))


class DatabaseConnection(object):
    def __init__(self, db_path):
        self.db_path = db_path
        self.execution_counter = 0
        try:
            self.connection = sqlite3.connect(db_path, check_same_thread=False)
            self.connection.row_factory = sqlite3.Row
            self.c = self.connection.cursor()
        except sqlite3.OperationalError:
            raise sqlite3.OperationalError("Couldn't connect to %s" % db_path)

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def execute(self, sql):
        return self.c.execute(sql)

    def execute_and_describe(self, sql):
        rows = self.c.execute(sql)
        description = [d[0] for d in self.c.description]
        return rows, description

    @property
    def parent_dir(self):
        return self.db_path.split("/")[-2]