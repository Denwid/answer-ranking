import nltk
import re


def wordtokens(s):
    return nltk.tokenize.word_tokenize(s)


def remove_stopwords(token_list):
    stopwords = nltk.corpus.stopwords.words('english')
    return [w for w in token_list if w.lower() not in stopwords]


def make_lowercase(token_list):
    return [w.lower() for w in token_list]


def count_syllables(word):
    """Simple syllable counter

    Makes lots of errors, but might be good enough.
    Taken from http://stackoverflow.com/questions/14541303/count-the-number-of-syllables-in-a-word
    """
    count = 0
    vowels = 'aeiouy'
    word = word.lower()
    if word[0] in vowels:
        count +=1
    for index in range(1,len(word)):
        if word[index] in vowels and word[index-1] not in vowels:
            count +=1
    if word.endswith('e'):
        count -= 1
    if word.endswith('le'):
        count+=1
    if count == 0:
        count +=1
    return count


def filter_out_rich_content(document):
    document = filter_out_latex(document)
    document = filter_out_code(document)
    document = filter_out_html_tags(document)
    return document


def filter_out_latex(s):
    # filters out all occurences of $ $$ \[ and \( style latex tags
    return re.sub(r'\$\$.*?\$\$|\$.*?\$|\\\[.*?\\\]|\\\(.*?\\\)', '', s)


def filter_out_code(s):
    return re.sub(r'<code>.*?</code>', '', s )


def filter_out_html_tags(s):
    return re.sub(r'<.*?>', '', s)


def filter_out_punctuation(s):
    return s.strip(".:;?!")


def wordlist(s, filter_rich_content=False, filter_punctuation=False, filter_stopwords=False):
    if filter_rich_content:
        s = filter_out_rich_content(s)
    if filter_punctuation:
        s = filter_out_punctuation(s)
    s = wordtokens(s)
    s = make_lowercase(s)
    if filter_stopwords:
        s = remove_stopwords(s)
    return s


def wordset(s, filter_rich_content=False, filter_punctuation=False):
    if filter_rich_content:
        s = filter_out_rich_content(s)
    if filter_punctuation:
        s = filter_out_punctuation(s)
    s = wordtokens(s)
    s = make_lowercase(s)
    return set(s)


def tags2list(tags):
    """Convert stackexchange tags to a list of tags

    For example:
        "<anova><chi-squared><generalized-linear-model>"
        returns
        ['anova', 'chi-squared', 'generalized-linear-model']
    """
    return re.findall('<([^>]*)>', tags)