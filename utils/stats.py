import numpy


def mean_and_std(vector):
    if vector is None:
        return None
    vector = numpy.array(vector)
    return (vector.mean(), vector.std())


def mean(l):
    if l == []:
        print "Hack warning: returning 0.0 as mean() of an empty list"
        return 0.0
    return float(sum(l))/len(l)


def min_max(data):
    if type(data) == list:
        minimum = min([min(s) for s in data])
        maximum = max([max(s) for s in data])
    else:
        minimum = min(data)
        maximum = max(data)
    return minimum, maximum


def norm_list(l):
    return [float(i)/sum(l) for i in l]


def norm_dict(d):
    factor=1.0/sum(d.itervalues())
    for k in d:
        d[k] = d[k]*factor
    return d